# LENG3
# 2016/2017

#Sets
set I; #Set of points of origin i (cities, plants, cooperatives, terminals or distribution centers)
set J; #Set of specialized terminals j (located in public sea ports or private terminals)
set K; #Set of points of destination k (maritime terminals)
set M; #Set of inputs m
set Q; #Set of products q
set S; #Set of services s

#Parameters of the model
param P{K,Q}; #Price of the product q in Q sold to destination k in K
param Cin{I,J,M}; #Cost of transporting input m in M from origin i in I to the terminal j in J
param Cout{J,K,Q}; #Cost of the transporting the product q in Q from the specialized terminal j in J to destination k in K
param f{J}; #Fixed cost of installation of a specialized terminal j in J
param gserv{J,S}; #Fixed cost to open the service s in S in the specialized terminal j in J
param alphaserv{J,S,M}; #Variable cost of the package of services s in S, applied to the input m in M in the specialized terminal j in J
param betaserv{J,S,M,Q}; #Change of volume coefficient by service s in S and input m in M
param w{J,S}; #Nominal service capacity of the service type s in S in terminal j in J
param W{J}; #Total capacity in the specialized terminal j in J
param Z; #Maximum number of opened specialized terminals
param Sup{I,M}; #Supply of inputs m in M at the origin i in I
param Dem{K,Q}; #Maximum demand for the product q in Q at destination k in K

#Decision variables
var Y{J} binary; # 1, if the specialized terminal is opened in location j; 0, otherwise
var Yserv{J,S} binary ; # 1, if the service s is offered at the location j; 0, otherwise
var Xin{I,J,S,M} >=0; # Quantity of the input m, assigned from origin i to the specialized terminal j, to receive the service s
var Xout{J,K,S,Q} >=0; # Quantity of product q that was subjected to the service s in the specialized terminal j with destination k

#Objective
# Sell revenew
# - Transportations costs from origin to terminal
# - Costs for specialized terminal and services
# - Transportations costs from origin to terminal

maximize Total_Profit: sum{k in K, q in Q, j in J, s in S} P[k,q]*Xout[j,k,s,q] 
               - sum{j in J}(sum{i in I, m in M}(Cin[i,j,m] * sum{s in S} Xin[i,j,s,m]))
               - sum{j in J}(f[j]*Y[j]+sum{s in S}(gserv[j,s] * Yserv[j,s] + sum{m in M}( alphaserv[j,s,m]*sum{i in I} Xin[i,j,s,m])))
               - sum{j in J,k in K, q in Q} ( Cout[j,k,q] * sum{s in S} Xout[j,k,s,q]);
               
#Restritions
# every input m supplied by every origin i directed to all services sin S in all terminals jin J 
# must be compatible with the supply of this input m at the given origin i.
subject to  InputSup {i in I, m in M}: sum{j in J, s in S} Xin[i,j,s,m] <= Sup[i,m]; 

# the quantity of product q from all terminals jin J must be equal or
# lower than the maximum demand of product q at destination k
subject to  ProdDem {k in K, q in Q} : sum{j in J, s in S} Xout[j,k,s,q] <= Dem[k,q];

# for each terminal j and each service package s the inbound flow of input m sent to such terminal 
# must be compatible with the nominal service capacity locally offered
subject to  TermServCap {j in J, s in S}: sum{i in I, m in M} Xin[i,j,s,m] <= w[j,s]*Yserv[j,s];

# the totalinputs m sent from all origins i to any given service s in terminals j 
# must be compatible with the capacity of the corresponding terminal j
subject to  TermCap {j in J}: sum{i in I, s in S, m in M} Xin[i,j,s,m] <= W[j]*Y[j];

# each input, each product, and each type of service the flow conservation equation 
# indicating the equilibrium between all amounts received and sent from the corresponding terminals
subject to  Balance {j in J, s in S, m in M, q in Q}: betaserv[j,s,m,q]*sum{i in I} Xin[i,j,s,m] = sum{k in K} Xout[j,k,s,q];

# limit of Z terminals to be opened
subject to  TermNum: sum{j in J} Y[j] <= Z;