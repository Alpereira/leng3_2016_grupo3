 package model;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author Henrique Melo <1150494@isep.ipp.pt>
 */
public class CustoTransporte1 {

    private Object locEspecializado;
    private Object locDestino;
    private Object m_oProduto;
    private double m_dCusto;
    private Date m_datDataEntrega;

    public CustoTransporte1() {
    }

    public CustoTransporte1(Object locEspecializado, Object locDestino, Object produto, double custo, Date data) {
        this.locEspecializado = locEspecializado;
        this.locDestino = locDestino;
        this.m_oProduto = produto;
        this.m_dCusto = custo;
        this.m_datDataEntrega = data;
    }

    public boolean valida() {
        // Escrever aqui o código de validação

        //
        return true;
    }

    public void setLocalEspecializado(Object locEspecializado) {
        this.locEspecializado = locEspecializado;
    }

    public void setLocalDestino(Object locDestino) {
        this.locDestino = locDestino;
    }

    public void setProduto(Object produto) {
        this.m_oProduto = produto;
    }

    public void setCusto(double custo) {
        this.m_dCusto = custo;
    }

    public void setDataEntrega(Date data) {
        this.m_datDataEntrega = data;
    }

    @Override
    public String toString() {
        return this.locEspecializado.toString() + ";" + this.locDestino.toString() + ";" + this.m_oProduto.toString() + ";" + this.m_dCusto + ";" + this.m_datDataEntrega + ";";
    }
     public static void gravar(ArrayList<CustoTransporte1> ct1) throws IOException {

        ObjectOutputStream out = new ObjectOutputStream(
                new FileOutputStream("CustoTransporteDerivados.bin"));
        try {
            out.writeObject(ct1);
            out.flush();
            out.close();
            System.out.println("Escrita concluida...");
        } catch (IOException e) {
            e.printStackTrace();
        }  
    }
}
