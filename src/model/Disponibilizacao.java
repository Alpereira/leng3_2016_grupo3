package model;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

/**
 *
 * @author Henrique Melo <1150494@isep.ipp.pt>
 */
public class Disponibilizacao {

    private Object m_oServico;
    private double custoServico;
    private int capacidadeNominal;

    public Disponibilizacao(Object m_oServico, double custoServico, int capacidadeNominal) {
        this.m_oServico = m_oServico;
        this.custoServico = custoServico;
        this.capacidadeNominal = capacidadeNominal;
    }

    public void setM_oServico(Object m_oServico) {
        this.m_oServico = m_oServico;
    }

    public void setCustoServico(double custoServico) {
        this.custoServico = custoServico;
    }

    public void setCapacidadeNominal(int capacidadeNominal) {
        this.capacidadeNominal = capacidadeNominal;
    }

    @Override
    public String toString() {
        return this.m_oServico.toString() + ";" + this.custoServico + ";" + this.capacidadeNominal + ";";
    }

    public Object getServico() {
        return this.m_oServico;
    }

    public double getCustoServico() {
        return custoServico;
    }

    public int getCapacidadeNominal() {
        return capacidadeNominal;
    }

    public static void gravar(ArrayList<Disponibilizacao> dis) throws IOException {

        ObjectOutputStream out = new ObjectOutputStream(
                new FileOutputStream("Disponibilizacao.bin"));
        try {
            out.writeObject(dis);
            out.flush();
            out.close();
            System.out.println("Escrita concluida...");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
