package model;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Henrique Melo <1150494@isep.ipp.pt>
 */
public class PeriodoColheita {

    private Date dataInicio;
    private Date dataFim;
    private List<Previsao> m_lstPrevisoes;

    public PeriodoColheita() {
        this.m_lstPrevisoes = new ArrayList<>();
    }

    public Date getDataInicio() {
        return dataInicio;
    }

    public void setDataInicio(Date dtInicio) {
        this.dataInicio = dtInicio;
    }

    public Date getDataFim() {
        return dataFim;
    }

    public void setDataFim(Date dtFim) {
        this.dataFim = dtFim;
    }

    public boolean valida() {

        return true;
    }

    @Override
    public String toString() {
        return this.dataInicio.toString() + ";" + this.dataFim.toString() + ";";
    }

    public Previsao novaPrevisao(LocalProducao local) {
        return new Previsao(local);
    }

    public boolean guardaPrevisao(Previsao prev) {
        if (validaPrevisao(prev)) {
            return this.m_lstPrevisoes.add(prev);
        }
        return false;
    }

    boolean hasPrevisao(LocalProducao local) {
        boolean bRet = false;
        for (Previsao prev : this.m_lstPrevisoes) {
            if (prev.iLocal(local)) {
                return true;
            }
        }

        return bRet;
    }

    private boolean validaPrevisao(Previsao prev) {
        boolean bRet = false;
        if (prev.valida()) {

            bRet = true;
        }
        return bRet;
    }

    public static void gravar(ArrayList<PeriodoColheita> periodo) throws IOException {

        ObjectOutputStream out = new ObjectOutputStream(
                new FileOutputStream("PeriodosColheita.bin"));
        try {
            out.writeObject(periodo);
            out.flush();
            out.close();
            System.out.println("Escrita concluida...");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
