package model;

/**
 *
 * @author Henrique Melo <1150494@isep.ipp.pt>
 */
public class Aplicacao {

    private Object m_oMateriaPrima;
    private Object m_oProdutoResultante;
    private double Racio;

    public Object getMateriaPrima() {
        return m_oMateriaPrima;
    }

    public void setMateriaPrima(Object oMateriaPrima) {
        this.m_oMateriaPrima = oMateriaPrima;
    }

    public Object getProdutoResultante() {
        return m_oProdutoResultante;
    }

    public void setProdutoResultante(Object oProdutoResultante) {
        this.m_oProdutoResultante = oProdutoResultante;
    }

    public double getRacio() {
        return Racio;
    }

    public void setRacio(double Racio) {
        this.Racio = Racio;
    }

    boolean valida() {
        return true;
    }

    @Override
    public String toString() {
        return this.m_oMateriaPrima + ";" + this.m_oProdutoResultante + ";" + this.Racio + ";";
    }

}
