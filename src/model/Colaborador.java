package model;

/**
 *
 * @author Henrique Melo <1150494@isep.ipp.pt>
 */
public class Colaborador {

    public String codigo;
    public int numero;
    public String nome;
    public String dataNascimento;
    public String email;

    public Colaborador() {
    }

    public Colaborador(String codigo,int numero, String nome, String dataNascimento, String email) {
        this.codigo = codigo;
        this.numero = numero;
        this.nome = nome;
        this.dataNascimento = dataNascimento;
        this.email = email;
    }

    public final void setCodigo(String sCodigo) {
        this.codigo = sCodigo;
    }
    
    public final void setID(int sNumero) {
        this.numero = sNumero;
    }

    public final void setNome(String sNome) {
        this.nome = sNome;
    }

    public final void setDataNascimento(String sDataNascimento) {
        this.dataNascimento = sDataNascimento;
    }

    public final void setEmail(String sEmail) {
        this.email = sEmail;
    }

    @Override
    public String toString() {
        return this.codigo + ";" + this.nome + ";" + this.numero + ";" + this.dataNascimento + ";" + this.email + ";";
    }

}
