package model;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

/**
 *
 * @author Henrique Melo <1150494@isep.ipp.pt>
 */
public class PrecoVenda {

    private String ano;
    private String preco;
    private String qntProcura;
    private Object local;
    private Object produto;

    public PrecoVenda() {
    }

    public PrecoVenda(Object loc, String ano, Object p, String preco, String qtd) {

        this.local = loc;
        this.ano = ano;
        this.produto = p;
        this.preco = preco;
        this.qntProcura = qtd;

    }

    public boolean valida() {
        // Escrever aqui o código de validação

        //
        return true;
    }

    public void setLocal(Object loc) {
        this.local = loc;
    }

    public void setAno(String ano) {
        this.ano = ano;
    }

    public void setProduto(Object p) {
        this.produto = p;
    }

    public void setPrecoVenda(String preco) {
        this.preco = preco;
    }

    public void setQuantidadeProcura(String qtd) {
        this.qntProcura = qtd;
    }

    @Override
    public String toString() {
        return this.local.toString() + ";" + this.produto.toString() + ";" + this.preco + ";" + this.ano +";" + this.qntProcura + ";";
    }
        public static void gravar(ArrayList<PrecoVenda> Pvenda) throws IOException {

        ObjectOutputStream out = new ObjectOutputStream(
                new FileOutputStream("PrecoVenda.bin"));
        try {
            out.writeObject(Pvenda);
            out.flush();
            out.close();
            System.out.println("Escrita concluida...");
        } catch (IOException e) {
            e.printStackTrace();
        }  
    }
}
