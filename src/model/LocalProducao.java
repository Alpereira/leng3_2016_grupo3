package model;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Formatter;
import java.util.List;
import java.util.Locale;
import java.util.Scanner;

/**
 *
 * @author Henrique Melo <1150494@isep.ipp.pt>
 */
public class LocalProducao {

    private int id;
    private String denominacao;
    private String abreviatura;
    private String endPostal;
    private String coordGPS;
    private final List<MateriaPrima> m_lstMatPrimas;
    private final List<Disponibilizacao> m_lstServicos;
    private final List<PossivelDisponibilizacao> m_lstServicosFuturos;
    private Object m_oMatPrima;
    private Object m_oResponsavelProducao;

    public LocalProducao() {
        this.m_lstMatPrimas = new ArrayList<>();
        this.m_lstServicosFuturos = new ArrayList<>();
        this.m_lstServicos = new ArrayList<>();
    }

    public LocalProducao(int id, String den, String abreviatura, String endPostal, String gps,Object col,Object m) {
        this.setID(id);
        this.setDenominacao(den);
        this.setAbreviatura(abreviatura);
        this.setEndereco(endPostal);
        this.setCoordGPS(gps);
        this.setResponsavelProducao(col);
        this.setMatPrima(m);
        this.m_lstMatPrimas = new ArrayList<>();
        this.m_lstServicos = new ArrayList<>();
        this.m_lstServicosFuturos = new ArrayList<>();
        this.abreviatura = abreviatura;

    }

    public final void setID(int id) {
        this.id = id;
    }


    public final void setDenominacao(String den) {
        this.denominacao = den;
    }

    public void setAbreviatura(String abreviatura) {
        this.abreviatura = abreviatura;
    }
     public void setMatPrima(Object m) {
        this.m_oMatPrima = m;
    }

    public final void setEndereco(String endPostal) {
        this.endPostal = endPostal;
    }

    public final void setCoordGPS(String gps) {
        this.coordGPS = gps;
    }

    public boolean valida() {
        // Escrever aqui o código de validação

        //
        return true;
    }

    public boolean isIdentifiableAs(int id) {
        return this.id == id;
    }

    @Override
    public String toString() {
        return this.abreviatura + ";"  + this.denominacao + ";" + this.endPostal + ";" + this.coordGPS + ";" + this.m_oResponsavelProducao + ";" + this.m_oMatPrima + ";";
    }

    public List<MateriaPrima> getMateriasPrimasProduzidas() {
        return m_lstMatPrimas;
    }

    public void addMateriaPrimaProduzida(MateriaPrima mp) {
        this.m_lstMatPrimas.add(mp);
    }
    
    public void setResponsavelProducao(Object c) {
        this.m_oResponsavelProducao = c;
    }

    public Object getResponsavelProducao() {
        return this.m_oResponsavelProducao;
    }
    public Object getMatPrima() {
        return this.m_oMatPrima;
    }

    public void addServico(Disponibilizacao servico) {
        this.m_lstServicos.add(servico);
    }

    public int getID() {
        return id;
    }

    public void addServicoFuturo(PossivelDisponibilizacao servico) {
        this.m_lstServicosFuturos.add(servico);
    }

    public List<Disponibilizacao> getM_lstServicos() {
        return m_lstServicos;
    }

    public List<PossivelDisponibilizacao> getM_lstServicosFuturos() {
        return m_lstServicosFuturos;
    }

    public static void gravar(ArrayList<LocalProducao> m_lstLocaisProducao) throws IOException {

        Formatter fout = new Formatter(new File("Produtos1.txt"));
     for (LocalProducao lp : m_lstLocaisProducao) {
         
             fout.format(Locale.ENGLISH,"%s;",lp.toString());
        
        fout.flush();
        fout.close();
        System.out.println("Escrita concluida...");
     
    }
    }
        public static ArrayList<LocalProducao> ler() throws FileNotFoundException, IOException, ClassNotFoundException {

        //leitura do ficheiro
        ArrayList<LocalProducao> m_lstLocaisProducao = new ArrayList<LocalProducao>();

        try {
            FileInputStream fin = new FileInputStream("LocaisProducao.bin");
            ObjectInputStream in = new ObjectInputStream(fin);

            m_lstLocaisProducao = (ArrayList<LocalProducao>) in.readObject();
            

            in.close();
            fin.close();
            System.out.println(m_lstLocaisProducao);
            System.out.println("Leitura concluida...");
        } catch (ClassNotFoundException | IOException | ClassCastException e) {
            e.printStackTrace();
        }
        return m_lstLocaisProducao;
    }
}

 

    

