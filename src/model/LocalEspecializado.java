package model;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Henrique Melo <1150494@isep.ipp.pt>
 */
public class LocalEspecializado {

    private int id;
    private String denominacao;
    public String m_sAbreviatura;
    private String endereco;
    private String coordGPS;
    public double custoInstalacao;
    public int capacidadeTotal;

    private final List<Disponibilizacao> m_lstServicos;

    public LocalEspecializado() {

        this.m_lstServicos = new ArrayList<>();
    }

    public void setID(int id) {
        this.id = id;
    }

    public void setDenominacao(String denominacao) {
        this.denominacao = denominacao;
    }

    public void setAbreviatura(String m_sAbreviatura) {
        this.m_sAbreviatura = m_sAbreviatura;
    }

    public void setEndereco(String endereco) {
        this.endereco = endereco;
    }

    public void setCoordGPS(String coordGPS) {
        this.coordGPS = coordGPS;
    }

    public void setCustoInstalacao(double custoInstalacao) {
        this.custoInstalacao = custoInstalacao;
    }

    public void setCapacidadeTotal(int capacidadeTotal) {
        this.capacidadeTotal = capacidadeTotal;
    }

    public void addServico(Disponibilizacao servico) {
        this.m_lstServicos.add(servico);
    }

    public boolean isIdentifiableAs(int iID) {
        return this.id == iID;
    }

    @Override
    public String toString() {
        String s = m_sAbreviatura + ";" + this.custoInstalacao + ";" + this.capacidadeTotal + ";" + this.denominacao + ";" + this.endereco + ";" + this.coordGPS + ";" ;
        String r = "";
        for (int i = 0; i < m_lstServicos.size(); i++) {
            r += m_lstServicos.get(i).toString();
        }
        return s + r;
    }
    

    public boolean valida() {

        return true;
    }

    public static void gravar(ArrayList<LocalEspecializado> locEsp) throws IOException {

        ObjectOutputStream out = new ObjectOutputStream(
                new FileOutputStream("LocaisEspecializados.bin"));
        try {
            out.writeObject(locEsp);
            out.flush();
            out.close();
            System.out.println("Escrita concluida...");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
