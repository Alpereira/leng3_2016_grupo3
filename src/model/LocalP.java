package model;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

/**
 *
 * @author Henrique Melo <1150494@isep.ipp.pt>
 */
public class LocalP {

    public Object m_oProduto;
    public double custoProduto;
    private double procuraPrevista;

    public LocalP(Object m_oProduto, double custoProduto, double procuraPrevista) {
        this.m_oProduto = m_oProduto;
        this.custoProduto = custoProduto;
        this.procuraPrevista = procuraPrevista;
    }

    public void setCustoProduto(double custoProduto) {
        this.custoProduto = custoProduto;
    }

    public void setMProcuraPrevista(double procuraPrevista) {
        this.procuraPrevista = procuraPrevista;
    }

    @Override
    public String toString() {
        return m_oProduto.toString() + ";" + custoProduto + ";" + procuraPrevista + ";";
    }

       public static void gravar(ArrayList<LocalP> custoProd) throws IOException {

        ObjectOutputStream out = new ObjectOutputStream(
                new FileOutputStream("CustoProdutos.bin"));
        try {
            out.writeObject(custoProd);
            out.flush();
            out.close();
            System.out.println("Escrita concluida...");
        } catch (IOException e) {
            e.printStackTrace();
        }  
    }
}
