package model;

/**
 *
 * @author Henrique Melo <1150494@isep.ipp.pt>
 */
public class FichaTecnica {

    private String m_sTexto;

    public String getDados() {
        return m_sTexto;
    }

    public void setDados(String m_sTexto) {
        this.m_sTexto = m_sTexto;
    }

}
