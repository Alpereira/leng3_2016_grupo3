package model;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Henrique Melo <1150494@isep.ipp.pt>
 */
public class Servico {

    public String codigo;
    private String descricao;
    private FichaTecnica fichaTecnica;
    private final List<Aplicacao> m_lstAplicacoes;

    public Servico() {
        this.m_lstAplicacoes = new ArrayList<>();
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String sCodigo) {
        this.codigo = sCodigo;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String sDescricao) {
        this.descricao = sDescricao;
    }

    public FichaTecnica getFichaTecnica() {
        return this.fichaTecnica;
    }

    public void setFichaTecnica(String sTexto) {
        this.fichaTecnica = new FichaTecnica();
        this.fichaTecnica.setDados(sTexto);
    }

    public boolean valida() {
        return true;
    }

    public boolean isIdentifiableAs(String sCodigo) {
        return this.codigo.equals(sCodigo);
    }

    @Override
    public String toString() {
        String apps = aplicacoestoString();
        return this.codigo + ";" + this.descricao + ";" + apps + ";";

    }

    public Aplicacao novaAplicacao() {
        return new Aplicacao();
    }

    public boolean guardaAplicacao(Aplicacao oApl) {
        if (this.validaAplicacao(oApl)) {
            return this.m_lstAplicacoes.add(oApl);
        }
        return false;
    }

    private boolean validaAplicacao(Aplicacao oApl) {
        boolean bRet = false;
        if (oApl.valida()) {

            bRet = true;
        }
        return bRet;
    }

    private String aplicacoestoString() {
        String sRet = "Aplicações:";
        for (Aplicacao apl : this.m_lstAplicacoes) {
            sRet += String.format("\n%s", apl.toString());
        }
        return sRet;
    }
    public static void gravar(ArrayList<Servico> ser) throws IOException {

        ObjectOutputStream out = new ObjectOutputStream(
                new FileOutputStream("Servicos.bin"));
        try {
            out.writeObject(ser);
            out.flush();
            out.close();
            System.out.println("Escrita concluida...");
        } catch (IOException e) {
            e.printStackTrace();
        }  
    }
}
