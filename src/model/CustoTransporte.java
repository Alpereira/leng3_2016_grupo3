package model;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author Henrique Melo <1150494@isep.ipp.pt>
 */
public class CustoTransporte {

    private Object locOrigem;
    private Object locEspecializado;
    private Object prod;
    private double m_dCusto;
    private Date m_datDataEntrega;

    public CustoTransporte() {
    }

    public CustoTransporte(Object locOrigem, Object locEspecializado, Object produto, double custo, Date data) {
        this.locOrigem = locOrigem;
        this.locEspecializado = locEspecializado;
        this.prod =  produto;
        this.m_dCusto = custo;
        this.m_datDataEntrega = data;
    }

    public boolean valida() {

        return true;
    }

    public void setLocalOrigem(Object locOrigem) {
        this.locOrigem = locOrigem;
    }

    public void setLocalEspecializado(Object locEspecializado) {
        this.locEspecializado = locEspecializado;
    }

    public void setProduto(Object produto) {
        this.prod = produto;
    }

    public void setCusto(double custo) {
        this.m_dCusto = custo;
    }

    public void setDataEntrega(Date data) {
        this.m_datDataEntrega = data;
    }

    @Override
    public String toString() {
        return this.locOrigem + ";" + this.locEspecializado + ";" + this.prod + ";" + this.m_dCusto + ";" + this.m_datDataEntrega + ";";
    
    }
     public static void gravar(ArrayList<CustoTransporte> ct) throws IOException {

        ObjectOutputStream out = new ObjectOutputStream(
                new FileOutputStream("CustoTransporteMaterias.bin"));
        try {
            out.writeObject(ct);
            out.flush();
            out.close();
            System.out.println("Escrita concluida...");
        } catch (IOException e) {
            e.printStackTrace();
        }  
    }
}
