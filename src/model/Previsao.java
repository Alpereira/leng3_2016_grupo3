package model;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

/**
 *
 * @author Henrique Melo <1150494@isep.ipp.pt>
 */
public class Previsao
{
    private LocalProducao m_oLocalProducao;
    private double quantidade; 
    
    
     boolean iLocal(LocalProducao local)
    {
        return this.m_oLocalProducao.equals(local);
    }
     
    public Previsao(LocalProducao local)
    {
        this.m_oLocalProducao = local;
    }

    public double getQuantidade()
    {
        return quantidade;
    }

    public void setQuantidade(double dQuantidade)
    {
        this.quantidade = dQuantidade;
    }
    
    boolean valida()
    {
        return true;
    }
    
    @Override
    public String toString()
    {
        return String.format("%s;%f\n",this.m_oLocalProducao.toString(), this.quantidade);
        
    }
        public static void gravar(ArrayList<Previsao> previsao) throws IOException {

        ObjectOutputStream out = new ObjectOutputStream(
                new FileOutputStream("Previsoes.bin"));
        try {
            out.writeObject(previsao);
            out.flush();
            out.close();
            System.out.println("Escrita concluida...");
        } catch (IOException e) {
            e.printStackTrace();
        }  
    }

}
