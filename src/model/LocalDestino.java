package model;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Henrique Melo <1150494@isep.ipp.pt>
 */
public class LocalDestino {

    public int id;
    private String denominacao;
    public String abreviatura;
    private String endereco;
    private String coordGPS;
    public double custoInstalacao;
    public int capacidadeTotal;

    private final List<LocalP> m_lstProdutos;

    public LocalDestino() {

        this.m_lstProdutos = new ArrayList<>();
    }

    public void setID(int id) {
        this.id = id;
    }

    public void setDenominacao(String denominacao) {
        this.denominacao = denominacao;
    }

    public void setAbreviatura(String abreviatura) {
        this.abreviatura = abreviatura;
    }

    public void setEndereco(String endereco) {
        this.endereco = endereco;
    }

    public void setCoordGPS(String coordGPS) {
        this.coordGPS = coordGPS;
    }

    public void setCustoInstalacao(double custoInstalacao) {
        this.custoInstalacao = custoInstalacao;
    }

    public void setCapacidadeTotal(int capacidadeTotal) {
        this.capacidadeTotal = capacidadeTotal;
    }

    public void addProduto(LocalP p) {
        this.m_lstProdutos.add(p);
    }

    public boolean isIdentifiableAs(int iID) {
        return this.id == iID;
    }

    @Override
    public String toString() {
        String s = this.abreviatura + ";" + this.denominacao + ";" + this.endereco + ";" + this.coordGPS + ";";
        String r = "";

        for (int i = 0; i < m_lstProdutos.size(); i++) {
            r += m_lstProdutos.get(i).toString();
        }
        return s + r;

    }

    public boolean valida() {

        return true;
    }
    
       public static void gravar(ArrayList<LocalDestino> locDes) throws IOException {

        ObjectOutputStream out = new ObjectOutputStream(
                new FileOutputStream("LocaisDestino.bin"));
        try {
            out.writeObject(locDes);
            out.flush();
            out.close();
            System.out.println("Escrita concluida...");
        } catch (IOException e) {
            e.printStackTrace();
        }  
    }

}
