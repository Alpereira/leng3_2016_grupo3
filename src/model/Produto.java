package model;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

/**
 *
 * @author Henrique Melo <1150494@isep.ipp.pt>
 */
public class Produto {

    public String id;
    private String descBreve;
    private String descCompleta;
    private String codAlfandegario;

    public String getID() {
        return id;
    }

    public String getDescricaoBreve() {
        return descBreve;
    }

    public String getDescricaoCompleta() {
        return descCompleta;
    }

    public String getCodigoAlfandegario() {
        return codAlfandegario;
    }

    public void setID(String sID) {
        this.id = sID;
    }

    public void setDescricaoBreve(String sDescBreve) {
        this.descBreve = sDescBreve;
    }

    public void setDescricaoCompleta(String sDescCompleta) {
        this.descCompleta = sDescCompleta;
    }

    public void setCodigoAlfandegario(String m_sCodigoAlfandegario) {
        this.codAlfandegario = m_sCodigoAlfandegario;
    }

    public boolean isIdentifiableAs(String sID) {
        return this.id.equals(sID);
    }

    public boolean valida() {
        return true;
    }

    @Override
    public String toString() {
        return this.id + ";" + this.descBreve + ";" + this.descCompleta + ";" + this.codAlfandegario + ";";
    }
        public static void gravar(ArrayList<MateriaPrima> mp) throws IOException {

        ObjectOutputStream out = new ObjectOutputStream(
                new FileOutputStream("Materias-Primas.bin"));
        try {
            out.writeObject(mp);
            out.flush();
            out.close();
            System.out.println("Escrita concluida...");
        } catch (IOException e) {
            e.printStackTrace();
        }  
    }
            public static void gravar2(ArrayList<ProdutoDerivado> pd) throws IOException {

        ObjectOutputStream out = new ObjectOutputStream(
                new FileOutputStream("ProdutosDerivados.bin"));
        try {
            out.writeObject(pd);
            out.flush();
            out.close();
            System.out.println("Escrita concluida...");
        } catch (IOException e) {
            e.printStackTrace();
        }  
    }
    

}
