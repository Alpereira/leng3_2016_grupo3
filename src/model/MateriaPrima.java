package model;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Henrique Melo <1150494@isep.ipp.pt>
 */
public class MateriaPrima extends Produto implements Serializable{

    private final List<PeriodoColheita> m_lstPeriodosColheita;

    public MateriaPrima() {
        super();
        this.m_lstPeriodosColheita = new ArrayList<>();
    }

    public PeriodoColheita addPeriodoColheita(Date dataInicio, Date dataFim) {

        PeriodoColheita pc = new PeriodoColheita();
        pc.setDataInicio(dataInicio);
        pc.setDataFim(dataFim);

        return pc;
    }

    public boolean validaPeriodoColheita(PeriodoColheita pc) {
        boolean bRet = false;
        if (pc.valida()) {

            bRet = true;
        }
        return bRet;
    }

    public boolean registaPeriodoColheita(PeriodoColheita pc) {
        boolean bRet = validaPeriodoColheita(pc);
        if (bRet) {
            bRet = this.m_lstPeriodosColheita.add(pc);
        }
        return bRet;
    }

    public List<PeriodoColheita> getPeriodosSemPrevisao(LocalProducao local) {
        List<PeriodoColheita> lst = new ArrayList<>();
        for (PeriodoColheita pc : this.m_lstPeriodosColheita) {
            if (!pc.hasPrevisao(local)) {
                lst.add(pc);
            }
        }

        return lst;
    }

    }

