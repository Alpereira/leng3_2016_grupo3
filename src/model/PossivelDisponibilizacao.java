package model;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

/**
 *
 * @author Henrique Melo <1150494@isep.ipp.pt>
 */
public class PossivelDisponibilizacao {

    private Servico m_oServico;
    private double custoServico;
    public int capacidadeNominal;

    public PossivelDisponibilizacao(Servico m_oServico, double custoServico) {
        this.m_oServico = m_oServico;
        this.custoServico = custoServico;
    }

    public void setM_oServico(Servico m_oServico) {
        this.m_oServico = m_oServico;
    }

    public void setCustoServico(double custoServico) {
        this.custoServico = custoServico;
    }

    @Override
    public String toString() {
        return this.m_oServico.toString() + ";" + this.custoServico + ";";
    }

    public Object getServico() {
        return this.m_oServico;
    }

    public double getCustoServico() {
        return custoServico;
    }

    public static void gravar(ArrayList<PossivelDisponibilizacao> Pdis) throws IOException {

        ObjectOutputStream out = new ObjectOutputStream(
                new FileOutputStream("PossivelDisponibilizacao.bin"));
        try {
            out.writeObject(Pdis);
            out.flush();
            out.close();
            System.out.println("Escrita concluida...");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
