package controller;

import java.util.Date;
import java.util.List;
import registos.registoGeral;
import model.MateriaPrima;
import model.PeriodoColheita;

/**
 *
 * @author Henrique Melo <1150494@isep.ipp.pt>
 */
public class DefinirPeriodosColheitaController {

    private final registoGeral rg;
    private MateriaPrima m_oMatPrima;

    public DefinirPeriodosColheitaController(registoGeral Rg) {
        this.rg = Rg;
    }

    public List<MateriaPrima> getListaMateriasPrimas() {
        return this.rg.getListaMateriasPrimas();
    }

    public void setMateriaPrima(MateriaPrima mp) {
        this.m_oMatPrima = mp;
    }

    public boolean addPeriodoColheita(Date dataInicio, Date dataFim) {
        boolean bRet = false;

        PeriodoColheita pc = this.m_oMatPrima.addPeriodoColheita(dataInicio, dataFim);
        if (this.m_oMatPrima.validaPeriodoColheita(pc)) {
            bRet = this.m_oMatPrima.registaPeriodoColheita(pc);
        }

        return bRet;
    }

}
