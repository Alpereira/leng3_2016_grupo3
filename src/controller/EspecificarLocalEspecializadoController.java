package controller;

import java.util.List;
import registos.registoGeral;
import model.LocalEspecializado;
import model.Servico;
import model.Disponibilizacao;

/**
 *
 * @author Henrique Melo <1150494@isep.ipp.pt>
 */
public class EspecificarLocalEspecializadoController {

    private final registoGeral rg;
    private LocalEspecializado m_oLocalEspecializado;

    public EspecificarLocalEspecializadoController(registoGeral Rg) {
        this.rg = Rg;
    }

    public void novoLocalEspecializado() {
        this.m_oLocalEspecializado = this.rg.novoLocalEspecializado();
    }

    public void setDados(int id, String denominacao, String abreviatura, String endereco, String coordGPS, double custo, int capacidade) {
        this.m_oLocalEspecializado.setID(id);
        this.m_oLocalEspecializado.setDenominacao(denominacao);
        this.m_oLocalEspecializado.setAbreviatura(abreviatura);
        this.m_oLocalEspecializado.setEndereco(endereco);
        this.m_oLocalEspecializado.setCoordGPS(coordGPS);
        this.m_oLocalEspecializado.setCustoInstalacao(custo);
        this.m_oLocalEspecializado.setCapacidadeTotal(capacidade);
    }

    public boolean registaLocalEspecializado() {
        return this.rg.registaLocalEspecializado(this.m_oLocalEspecializado);
    }

    public String getLocalEspecializadoAsString() {
        return this.m_oLocalEspecializado.toString();
    }

    public List<Servico> getServicos() {
        return rg.getListaServicos();
    }

    public void addServico(Disponibilizacao s) {
        this.m_oLocalEspecializado.addServico(s);
    }

    public List<LocalEspecializado> getLocalEspecializado() {

        return this.rg.getListaLocaisEspecializados();
    }

}
