package controller;

import java.util.List;
import model.*;
import registos.registoGeral;


/**
 *
 * @author Henrique Melo <1150494@isep.ipp.pt>
 */
public class OptimizarCustoTransporteController {

    public LocalDestino m_oLocalDestino;
    public LocalP m_oProdLocal;
    public Servico m_oServico;
    public LocalEspecializado m_oLocalEspecializado;
    public MateriaPrima m_oMatPrima;
    public ProdutoDerivado m_oProdDerivado;
    public LocalProducao m_oLocalProducao;
    public registoGeral rg;
    

 

    public OptimizarCustoTransporteController(registoGeral Rg) {
        this.rg = Rg;
    }

    public List<LocalProducao> getListLocaisProducao() {

        return this.rg.getListaLocaisProducao();
    }

    public List<LocalDestino> getListLocaisDestino() {

        return this.rg.getListaLocaisDestino();
    }
    public List<PrecoVenda> getListPrecoVenda() {

        return this.rg.getListaPrecosVenda();
    }
    public List<LocalEspecializado> getListLocaisEspecializados() {

        return this.rg.getListaLocaisEspecializados();
    }

    public List<MateriaPrima> getListMateriasPrimas() {

        return this.rg.getListaMateriasPrimas();
    }

    public List<ProdutoDerivado> getListProdsDerivados() {

        return this.rg.getListaProdutosDerivados();
    }

    public List<CustoTransporte> getListCustoTransporte() {

        return this.rg.getListaCustoTransporte();
    }

    public List<CustoTransporte1> getListCustoTransporte1() {

        return this.rg.getListaCustoTransporte1();
    }

    public List<Servico> getListServicos() {
        return this.rg.getListaServicos();
    }

}
