package controller;

import java.util.List;
import model.Aplicacao;
import registos.registoGeral;
import model.LocalProducao;
import model.MateriaPrima;
import model.Produto;
import model.ProdutoDerivado;
import model.Servico;

/**
 *
 * @author Henrique Melo <1150494@isep.ipp.pt>
 */
public class DefinirServicoController {

    private final registoGeral rg;
    private Servico m_oServico;
    private Aplicacao m_oApl;

    public DefinirServicoController(registoGeral Rg) {
        this.rg = Rg;
    }

    public void novoServico() {
        this.m_oServico = this.rg.novoServico();
    }

    public Object setDados(String codigo, String descricao, String fichaTecnica) {
        this.m_oServico.setCodigo(codigo);
        this.m_oServico.setDescricao(descricao);
        this.m_oServico.setFichaTecnica(fichaTecnica);

        return this.rg.getProdutos();
    }

    public List<MateriaPrima> getMateriaPrima() {
        return this.rg.getListaMateriasPrimas();
    }

    public List<ProdutoDerivado> getProdutosDerivados() {
        return this.rg.getListaProdutosDerivados();
    }

    public void setMateriaPrima(Object mp) {
        this.m_oApl = this.m_oServico.novaAplicacao();
        this.m_oApl.setMateriaPrima(mp);
    }

    public void setProdutoResultante(Object pr) {
        this.m_oApl.setProdutoResultante(pr);
    }

    public boolean setRacio(double racio) {
        this.m_oApl.setRacio(racio);
        return this.m_oServico.guardaAplicacao(this.m_oApl);
    }

    public boolean validaServico() {
        return this.rg.validaServico(this.m_oServico);
    }

    public boolean registaServico() {
        return this.rg.registaServico(this.m_oServico);
    }

    public String getServicoAsString() {
        return this.m_oServico.toString();
    }

    public List<LocalProducao> getLocaisProducao() {
        return this.rg.getListaLocaisProducao();
    }

    public void addLocal(LocalProducao localProducao, double custo, int capacidade) {
        rg.addServicoALocal(localProducao, m_oServico, custo, capacidade);
    }

    public void addLocalFuturo(LocalProducao localProducao, double custo) {
        rg.addServicoALocalFuturo(localProducao, m_oServico, custo);
    }

    public void locaisComServiçoToString() {
        List<LocalProducao> lst_locais = rg.getListaLocaisProducao();
        for (int i = 0; i < lst_locais.size(); i++) {
            if (lst_locais.get(i).getM_lstServicos() != null) {
                for (int j = 0; j < lst_locais.get(i).getM_lstServicos().size(); j++) {
                    Servico s = (Servico) lst_locais.get(i).getM_lstServicos().get(j).getServico();
                    if (m_oServico.getCodigo().equalsIgnoreCase(s.getCodigo())) {
                        System.out.println(lst_locais.get(i).toString());
                        System.out.println("\nCusto do Serviço: " + lst_locais.get(i).getM_lstServicos().get(j).getCustoServico());
                        System.out.println("\nCapacidade Nominal: " + lst_locais.get(i).getM_lstServicos().get(j).getCapacidadeNominal());
                    }
                }
            }
        }
    }

    public void locaisComServiçoFuturoToString() {
        List<LocalProducao> lst_locais = rg.getListaLocaisProducao();
        for (int i = 0; i < lst_locais.size(); i++) {
            if (lst_locais.get(i).getM_lstServicos() != null) {
                for (int j = 0; j < lst_locais.get(i).getM_lstServicosFuturos().size(); j++) {
                    Servico s = (Servico) lst_locais.get(i).getM_lstServicosFuturos().get(j).getServico();
                    if (m_oServico.getCodigo().equalsIgnoreCase(s.getCodigo())) {
                        System.out.println(lst_locais.get(i).toString());
                        System.out.println("\nCusto do Serviço: " + lst_locais.get(i).getM_lstServicosFuturos().get(j).getCustoServico());
                    }
                }
            }
        }
    }
}
