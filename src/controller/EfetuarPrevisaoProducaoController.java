package controller;

import java.util.List;
import registos.registoGeral;
import model.LocalProducao;
import model.MateriaPrima;
import model.PeriodoColheita;
import model.Previsao;

/**
 *
 * @author Henrique Melo <1150494@isep.ipp.pt>
 */
public class EfetuarPrevisaoProducaoController {

    private final registoGeral rg;
    private LocalProducao m_oLocal;
    public MateriaPrima m_oMatPrima;

    public EfetuarPrevisaoProducaoController(registoGeral Rg) {
        this.rg = Rg;
    }

    public List<LocalProducao> getLocaisProducao(String user) {
        return this.rg.getLocaisProducao(user);
    }

    public List<MateriaPrima> getMateriaPrimasLocProd(LocalProducao local) {
        this.m_oLocal = local;
        return local.getMateriasPrimasProduzidas();
    }

    public List<PeriodoColheita> getPeriodosColheitaSemPrevisao(MateriaPrima mp) {
        this.m_oMatPrima = mp;
        return mp.getPeriodosSemPrevisao(this.m_oLocal);
    }

    public void setMateriaPrima(MateriaPrima mp) {
        this.m_oMatPrima = mp;
    }

    public boolean addPrevisao(PeriodoColheita pc, double qtd) {

        Previsao prev = pc.novaPrevisao(m_oLocal);
        prev.setQuantidade(qtd);

        return pc.guardaPrevisao(prev);

    }
}
