package controller;

import model.Aplicacao;
import model.PrecoVenda;
import registos.registoGeral;

/**
 *
 * @author Henrique Melo <1150494@isep.ipp.pt>
 */
public class EstipularPrecoVendaEmLocalController {

    public registoGeral rg;
    public PrecoVenda m_oPrecoVenda;
    public Aplicacao m_oApl;

    public EstipularPrecoVendaEmLocalController(registoGeral Rg) {
        this.rg = Rg;

    }

    public void novoPrecoVenda() {
        this.m_oPrecoVenda = this.rg.novoPrecoVenda();
    }

    public void setDados(Object loc, String ano, Object p, String preco, String qtd) {

        this.m_oPrecoVenda.setLocal(loc);
        this.m_oPrecoVenda.setAno(ano);
        this.m_oPrecoVenda.setProduto(p);
        this.m_oPrecoVenda.setPrecoVenda(preco);
        this.m_oPrecoVenda.setQuantidadeProcura(qtd);
    }

    public boolean registaPrecoVenda() {
        return this.rg.registaPrecoVenda(this.m_oPrecoVenda);
    }

    public String getPrecoVendaAsString() {
        return this.m_oPrecoVenda.toString();
    }
}
