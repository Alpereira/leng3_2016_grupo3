package controller;

import java.util.List;
import model.Colaborador;
import registos.registoGeral;
import model.LocalProducao;
import model.MateriaPrima;

/**
 *
 * @author Henrique Melo <1150494@isep.ipp.pt>
 */
public class EspecificarLocalProducaoController {

    private final registoGeral rg;
    private LocalProducao m_oLocalProducao;

    public EspecificarLocalProducaoController(registoGeral Rg) {
        this.rg = Rg;
    }

    public void novoLocalProducao() {
        this.m_oLocalProducao = this.rg.novoLocalProducao();
    }

    public void setDados(int id, String deno, String abrv, String endPostal, String gps, Object col,Object m) {
        this.m_oLocalProducao.setID(id);
        this.m_oLocalProducao.setDenominacao(deno);
        this.m_oLocalProducao.setAbreviatura(abrv);
        this.m_oLocalProducao.setEndereco(endPostal);
        this.m_oLocalProducao.setCoordGPS(gps);
        this.m_oLocalProducao.setResponsavelProducao(m);
        this.m_oLocalProducao.setMatPrima(m);
    }

    public boolean registaLocalProducao() {
        return this.rg.registaLocalProducao(this.m_oLocalProducao);
    }

    

    public String getLocalProducaoAsString() {
        return this.m_oLocalProducao.toString();
    }

    public List<Colaborador> getColaboradores() {
        return this.rg.getResponsaveisProducao();
    }

    public List<MateriaPrima> getMateriasPrimas() {
        return this.rg.getListaMateriasPrimas();
    }
    

}
