package controller;

import java.util.Date;
import java.util.List;
import model.Aplicacao;
import model.CustoTransporte1;
import model.LocalDestino;
import model.LocalEspecializado;
import model.ProdutoDerivado;
import registos.registoGeral;

/**
 *
 * @author Henrique Melo <1150494@isep.ipp.pt>
 */
public class AtualizarCustoTransporte1Controller {

    private final registoGeral rg;
    public CustoTransporte1 m_oCustoTransporte1;
    public Aplicacao m_oApl;

    public AtualizarCustoTransporte1Controller(registoGeral Rg) {
        this.rg = Rg;
    }

    public void novoCustoTransporte1() {
        this.m_oCustoTransporte1 = this.rg.novoCustoTransporte1();
    }

    public List<LocalDestino> getLocaisDestino() {
        return this.rg.getListaLocaisDestino();
    }

    public List<LocalEspecializado> getLocaisEspecializados() {
        return this.rg.getListaLocaisEspecializados();
    }

    public List<ProdutoDerivado> getDerivados() {
        return this.rg.getDerivados();
    }

    public void setDados1(Object especializado, Object destino, Object produto, double custo, Date data) {
        this.m_oCustoTransporte1.setLocalEspecializado(especializado);
        this.m_oCustoTransporte1.setLocalDestino(destino);
        this.m_oCustoTransporte1.setProduto(produto);
        this.m_oCustoTransporte1.setCusto(custo);
        this.m_oCustoTransporte1.setDataEntrega(data);
    }

    public boolean registaCustoTransporte1() {
        return this.rg.registaCustoTransporte1(this.m_oCustoTransporte1);
    }

    public String getCustoTransporte1AsString() {
        return this.m_oCustoTransporte1.toString();
    }
}
