package controller;

import registos.registoGeral;
import model.ProdutoDerivado;

/**
 *
 * @author Henrique Melo <1150494@isep.ipp.pt>
 */
public class EspecificarProdutoDerivadoController {

    private final registoGeral rg;
    private ProdutoDerivado m_oProdutoDerivado;

    public EspecificarProdutoDerivadoController(registoGeral Rg) {
        this.rg = Rg;
    }

    public void novoProdutoDerivado() {
        this.m_oProdutoDerivado = this.rg.novoProdutoDerivado();
    }

    public void setDados(String id, String dBreve, String dCompleta, String codAlfan) {
        this.m_oProdutoDerivado.setID(id);
        this.m_oProdutoDerivado.setDescricaoBreve(dBreve);
        this.m_oProdutoDerivado.setDescricaoCompleta(dCompleta);
        this.m_oProdutoDerivado.setCodigoAlfandegario(codAlfan);
    }

    public boolean registaProdutoDerivado() {
        return this.rg.registaProdutoDerivado(this.m_oProdutoDerivado);
    }

    public String getProdutoDerivadoAsString() {
        return this.m_oProdutoDerivado.toString();
    }

}
