package controller;

import java.util.Date;
import java.util.List;
import model.Aplicacao;
import registos.registoGeral;
import model.LocalProducao;
import model.CustoTransporte;
import model.LocalEspecializado;
import model.MateriaPrima;

/**
 *
 * @author Henrique Melo <1150494@isep.ipp.pt>
 */
public class AtualizarCustoTransporteController {

    private final registoGeral rg;
    private CustoTransporte m_oCustoTransporte;
    public Aplicacao m_oApl;

    public AtualizarCustoTransporteController(registoGeral Rg) {
        this.rg = Rg;
    }

    public void novoCustoTransporte() {
        this.m_oCustoTransporte = this.rg.novoCustoTransporte();
    }

    public List<LocalProducao> getLocaisProducao() {
        return this.rg.getListaLocaisProducao();
    }

    public List<LocalEspecializado> getLocaisEspecializados() {
        return this.rg.getListaLocaisEspecializados();
    }

    public List<MateriaPrima> getMateria() {
        return this.rg.getMateria();
    }

    public void setDados(Object origem, Object especializado, Object produto, double custo, Date data) {
        this.m_oCustoTransporte.setLocalOrigem(origem);
        this.m_oCustoTransporte.setLocalEspecializado(especializado);
        this.m_oCustoTransporte.setProduto(produto);
        this.m_oCustoTransporte.setCusto(custo);
        this.m_oCustoTransporte.setDataEntrega(data);
    }

    public boolean registaCustoTransporte() {
        return this.rg.registaCustoTransporte(this.m_oCustoTransporte);
    }

    public String getCustoTransporteAsString() {
        return this.m_oCustoTransporte.toString();
    }
}
