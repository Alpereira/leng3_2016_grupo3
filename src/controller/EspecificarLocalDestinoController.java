package controller;

import java.util.List;
import registos.registoGeral;
import model.LocalDestino;
import model.Produto;
import model.LocalP;

/**
 *
 * @author Henrique Melo <1150494@isep.ipp.pt>
 */
public class EspecificarLocalDestinoController {

    private final registoGeral rg;
    private LocalDestino m_oLocalDestino;

    public EspecificarLocalDestinoController(registoGeral Rg) {
        this.rg = Rg;
    }

    public void novoLocalDestino() {
        this.m_oLocalDestino = this.rg.novoLocalDestino();
    }

    public void setDados(int id, String denominacao, String abreviatura, String endereco, String coordGPS, double custo, int capacidade) {
        this.m_oLocalDestino.setID(id);
        this.m_oLocalDestino.setDenominacao(denominacao);
        this.m_oLocalDestino.setAbreviatura(abreviatura);
        this.m_oLocalDestino.setEndereco(endereco);
        this.m_oLocalDestino.setCoordGPS(coordGPS);
        this.m_oLocalDestino.setCustoInstalacao(custo);
        this.m_oLocalDestino.setCapacidadeTotal(capacidade);
    }

    public boolean registaLocalDestino() {
        return this.rg.registaLocalDestino(this.m_oLocalDestino);
    }

    public String getLocalDestinoAsString() {
        return this.m_oLocalDestino.toString();
    }

    public List<Produto> getProdutos() {
        return rg.getProdutos();
    }

    public void addProduto(LocalP p) {
        this.m_oLocalDestino.addProduto(p);
    }

    public List<LocalDestino> getLocalDestino() {

        return this.rg.getListaLocaisDestino();
    }
}
