package controller;

import registos.registoGeral;
import model.MateriaPrima;

/**
 *
 * @author Henrique Melo <1150494@isep.ipp.pt>
 */
public class EspecificarMateriaPrimaController {

    private final registoGeral rg;
    private MateriaPrima m_oMateriaPrima;

    public EspecificarMateriaPrimaController(registoGeral Rg) {
        this.rg = Rg;
    }

    public void novaMateriaPrima() {
        this.m_oMateriaPrima = this.rg.novaMateriaPrima();
    }

    public void setDados(String id, String dBreve, String dComp, String codAlfan) {
        this.m_oMateriaPrima.setID(id);
        this.m_oMateriaPrima.setDescricaoBreve(dBreve);
        this.m_oMateriaPrima.setDescricaoCompleta(dComp);
        this.m_oMateriaPrima.setCodigoAlfandegario(codAlfan);
    }

    public boolean registaMateriaPrima() {
        return this.rg.registaMateriaPrima(this.m_oMateriaPrima);
    }

    public String getMateriaPrimaAsString() {
        return this.m_oMateriaPrima.toString();
    }
}
