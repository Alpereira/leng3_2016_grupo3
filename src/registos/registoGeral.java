package registos;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import model.*;

/**
 *
 * @author Henrique Melo <1150494@isep.ipp.pt>
 */
public class registoGeral {

    public List<LocalProducao> m_lstLocaisProducao;
    public List<MateriaPrima> m_lstMateriasPrimas;
    public List<ProdutoDerivado> m_lstProdDerivados;
    public List<Servico> m_lstServicos;
    public List<Servico> m_lstServicosFuturos;
    public List<Colaborador> m_lstColaboradores;
    public List<LocalEspecializado> m_lstLocaisEspecializados;
    public List<LocalDestino> m_lstLocaisDestino;
    public List<CustoTransporte> m_lstCustoTransporte;
    public List<CustoTransporte1> m_lstCustoTransporte1;
    public List<PrecoVenda> m_lstPrecoVenda;
    public List<LocalProducao> m_lstLPabreviatura;

    public registoGeral() {
        this.m_lstLocaisProducao = new ArrayList<>();
        this.m_lstMateriasPrimas = new ArrayList<>();
        this.m_lstProdDerivados = new ArrayList<>();
        this.m_lstServicos = new ArrayList<>();
        this.m_lstServicosFuturos = new ArrayList<>();
        this.m_lstColaboradores = new ArrayList<>();
        this.m_lstLocaisEspecializados = new ArrayList<>();
        this.m_lstLocaisDestino = new ArrayList<>();
        this.m_lstCustoTransporte = new ArrayList<>();
        this.m_lstCustoTransporte1 = new ArrayList<>();
        this.m_lstPrecoVenda = new ArrayList<>();
        this.m_lstLPabreviatura = new ArrayList<>();

        parametrosExemplo();

    }

    //PREÇO DE VENDA
    public boolean registaPrecoVenda(PrecoVenda pv) {
        if (this.validaPrecoVenda(pv)) {
            return addPrecoVenda(pv);
        }
        return false;
    }

    public boolean validaPrecoVenda(PrecoVenda pv) {
        boolean bRet = false;
        if (pv.valida()) {
            // Escrever aqui o código de validação

            //
            bRet = true;
        }
        return bRet;
    }

    public boolean addPrecoVenda(PrecoVenda pv) {
        return m_lstPrecoVenda.add(pv);
    }

    public PrecoVenda novoPrecoVenda() {
        return new PrecoVenda();
    }

    /* Locais de Produção */
    public LocalProducao novoLocalProducao() {
        return new LocalProducao();
    }



    public boolean validaLocalProducao(LocalProducao lp) {
        boolean bRet = false;
        if (lp.valida()) {
            // Escrever aqui o código de validação

            //
            bRet = true;
        }
        return bRet;
    }



    public boolean registaLocalProducao(LocalProducao lp) {
        if (this.validaLocalProducao(lp)) {
            adicionaTodasMateriasPrimasALocal(lp);
            return addLocalProducao(lp);
        }
        return false;
    }

  

    public boolean addLocalProducao(LocalProducao lp) {
        return m_lstLocaisProducao.add(lp);
    }

    public boolean addLPabrv(LocalProducao a) {
        return m_lstLPabreviatura.add(a);
    }

    public List<LocalProducao> getListaLocaisProducao() {
        return this.m_lstLocaisProducao;
    }


    public List<LocalDestino> getListaLocaisDestino() {
        return this.m_lstLocaisDestino;
    }
     public List<PrecoVenda> getListaPrecosVenda() {
        return this.m_lstPrecoVenda;
    }

    public List<LocalEspecializado> getListaLocaisEspecializados() {
        return this.m_lstLocaisEspecializados;
    }

    public LocalProducao getLocalProducao(int iID) {
        for (LocalProducao local : this.m_lstLocaisProducao) {
            if (local.isIdentifiableAs(iID)) {
                return local;
            }
        }

        return null;
    }

    public LocalProducao getLPabrv(int abrv) {
        for (LocalProducao a : this.m_lstLPabreviatura) {
            if (a.isIdentifiableAs(abrv)) {
                return a;
            }
        }

        return null;
    }

    /* Matérias-Primas */
    public MateriaPrima novaMateriaPrima() {
        return new MateriaPrima();
    }

    public boolean validaMateriaPrima(MateriaPrima mp) {
        boolean bRet = false;
        if (mp.valida()) {
            // Escrever aqui o código de validação

            //
            bRet = true;
        }
        return bRet;
    }

    public boolean registaMateriaPrima(MateriaPrima mp) {
        if (this.validaMateriaPrima(mp)) {
            return addMateriaPrima(mp);
        }
        return false;
    }

    public boolean addMateriaPrima(MateriaPrima mp) {
        boolean bRet = m_lstMateriasPrimas.add(mp);
        adicionaMateriaPrimaTodosLocais(mp);
        return bRet;
    }

    public List<MateriaPrima> getListaMateriasPrimas() {
        return this.m_lstMateriasPrimas;
    }

    public MateriaPrima getMateriaPrima(String sID) {
        for (MateriaPrima mp : this.m_lstMateriasPrimas) {
            if (mp.isIdentifiableAs(sID)) {
                return mp;
            }
        }

        return null;
    }

    /* Produtos Derivados */
    public ProdutoDerivado novoProdutoDerivado() {
        return new ProdutoDerivado();
    }

    public boolean validaProdutoDerivado(ProdutoDerivado pd) {
        boolean bRet = false;
        if (pd.valida()) {
            // Escrever aqui o código de validação

            //
            bRet = true;
        }
        return bRet;
    }

    public boolean registaProdutoDerivado(ProdutoDerivado pd) {
        if (this.validaProdutoDerivado(pd)) {
            return addProdutoDerivado(pd);
        }
        return false;
    }

    public boolean addProdutoDerivado(ProdutoDerivado pd) {
        return m_lstProdDerivados.add(pd);
    }

    public List<ProdutoDerivado> getListaProdutosDerivados() {
        return this.m_lstProdDerivados;
    }

    public ProdutoDerivado getProdutoDerivado(String sID) {
        for (ProdutoDerivado pd : this.m_lstProdDerivados) {
            if (pd.isIdentifiableAs(sID)) {
                return pd;
            }
        }

        return null;
    }

    public List<Produto> getProdutos() {
        List<Produto> lst = new ArrayList<>();
        lst.addAll(this.m_lstMateriasPrimas);
        lst.addAll(this.m_lstProdDerivados);
        return lst;
    }

    public List<MateriaPrima> getMateria() {
        return m_lstMateriasPrimas;
    }

    public List<ProdutoDerivado> getDerivados() {
        return m_lstProdDerivados;
    }

    /* Servicos */
    public Servico novoServico() {
        return new Servico();
    }

    public Servico novoServicoFuturo() {
        return new Servico();
    }

    public boolean validaServico(Servico srv) {
        boolean bRet = false;
        if (srv.valida()) {
            // Escrever aqui o código de validação

            //
            bRet = true;
        }
        return bRet;
    }

    public boolean validaServicoFuturo(Servico srv) {
        boolean bRet = false;
        if (srv.valida()) {
            // Escrever aqui o código de validação

            //
            bRet = true;
        }
        return bRet;
    }

    public boolean registaServico(Servico srv) {
        if (this.validaServico(srv)) {
            return addServico(srv);
        }
        return false;
    }

    public boolean registaServicoFuturo(Servico srv) {
        if (this.validaServicoFuturo(srv)) {
            return addServicoFuturo(srv);
        }
        return false;
    }

    public boolean addServico(Servico srv) {
        return m_lstServicos.add(srv);
    }

    public boolean addServicoFuturo(Servico srv) {
        return m_lstServicosFuturos.add(srv);
    }

    public List<Servico> getListaServicos() {
        return this.m_lstServicos;
    }

    public List<Servico> getListaServicosFuturos() {
        return this.m_lstServicosFuturos;
    }

    public Servico getServico(String sCodigo) {
        for (Servico srv : this.m_lstServicos) {
            if (srv.isIdentifiableAs(sCodigo)) {
                return srv;
            }
        }

        return null;
    }

    public Servico getServicosFuturos(String sCodigo) {
        for (Servico srv : this.m_lstServicosFuturos) {
            if (srv.isIdentifiableAs(sCodigo)) {
                return srv;
            }
        }

        return null;
    }

    public List<LocalProducao> getLocaisProducao(String user) {

        return this.m_lstLocaisProducao;
    }

    public List<LocalProducao> getLPabrv(String user) {

        return this.m_lstLPabreviatura;
    }

    // Usado temporariamente enquanto nenhum UC especificar que MP os Locais Produzem
    public void adicionaMateriaPrimaTodosLocais(MateriaPrima mp) {
        for (LocalProducao l : this.m_lstLocaisProducao) {
            l.addMateriaPrimaProduzida(mp);
        }
    }

    public void adicionaMateriaPrimaTodosLocais1(MateriaPrima mp) {
        for (LocalProducao l : this.m_lstLPabreviatura) {
            l.addMateriaPrimaProduzida(mp);
        }
    }

    // Usado temporariamente enquanto nenhum UC especificar que MP os Locais Produzem
    public void adicionaTodasMateriasPrimasALocal(LocalProducao local) {
        for (MateriaPrima mp : this.m_lstMateriasPrimas) {
            local.addMateriaPrimaProduzida(mp);
        }
    }

    public static void lerColaboradores() throws FileNotFoundException, IOException, ClassNotFoundException {

        String fileNameDefined = "colaboradores.csv";
        // -File class needed to turn stringName to actual file
        File file = new File(fileNameDefined);

        try {
            // -read from filePooped with Scanner class
            Scanner inputStream = new Scanner(file);
            // hashNext() loops line-by-line
            while (inputStream.hasNext()) {
                //read single line, put in string
                String data = inputStream.next();
                System.out.println(data);

            }
            // after loop, close scanner
            inputStream.close();

        } catch (FileNotFoundException e) {

            e.printStackTrace();
        }

    }

    public void lerColaboradoresDeFicheiro(String file) {

        BufferedReader br = null;
        String line = "";
        String cvsSplitBy = ";";

        try {

            br = new BufferedReader(new FileReader(file));
            //salta primeira linha
            line = br.readLine();
            while ((line = br.readLine()) != null) {

                String[] variaveis = line.split(cvsSplitBy);
                Colaborador c = new Colaborador(variaveis[0],Integer.parseInt(variaveis[1]), variaveis[2], variaveis[3], variaveis[4]);

                m_lstColaboradores.add(c);
                System.out.println("Colaborador adicionado: ");
                System.out.println(c.toString());
            }

        } catch (FileNotFoundException e) {
        } catch (IOException e) {
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                }
            }
        }
    }

    public List<Colaborador> getResponsaveisProducao() {
        return m_lstColaboradores;
    }

    public void addServicoALocal(Object iLocalProducaoID, Servico m_oServico, double custo, int capacidade) {
        for (LocalProducao l : this.m_lstLocaisProducao) {
            if (l == iLocalProducaoID) {
                Disponibilizacao s = new Disponibilizacao(m_oServico, custo, capacidade);
                l.addServico(s);
                break;
            }
        }
    }

    public void addServicoALocalFuturo(Object iLocalProducaoID, Servico m_oServico, double custo) {
        for (LocalProducao l : this.m_lstLocaisProducao) {
            if (l == iLocalProducaoID) {
                PossivelDisponibilizacao s1 = new PossivelDisponibilizacao(m_oServico, custo);
                l.addServicoFuturo(s1);
                break;
            }
        }
    }

    public LocalEspecializado novoLocalEspecializado() {
        return new LocalEspecializado();
    }

    public LocalDestino novoLocalDestino() {
        return new LocalDestino();
    }

    public boolean validaLocalEspecializado(LocalEspecializado local) {
        boolean bRet = false;
        if (local.valida()) {
            // Escrever aqui o código de validação

            //
            bRet = true;
        }
        return bRet;
    }

    public boolean validaLocalDestino(LocalDestino local) {
        boolean bRet = false;
        if (local.valida()) {
            // Escrever aqui o código de validação

            //
            bRet = true;
        }
        return bRet;
    }

    public boolean registaLocalEspecializado(LocalEspecializado local) {
        if (this.validaLocalEspecializado(local)) {
            return addLocalEspecializado(local);
        }
        return false;
    }

    public boolean registaLocalDestino(LocalDestino local) {
        if (this.validaLocalDestino(local)) {
            return addLocalDestino(local);
        }
        return false;
    }

    public boolean addLocalEspecializado(LocalEspecializado local) {
        return m_lstLocaisEspecializados.add(local);
    }

    public boolean addLocalDestino(LocalDestino local) {
        return m_lstLocaisDestino.add(local);
    }

    public LocalDestino getLocalDestino(int iID) {
        for (LocalDestino local : this.m_lstLocaisDestino) {
            if (local.isIdentifiableAs(iID)) {
                return local;
            }
        }

        return null;
    }

    public LocalEspecializado getLocalEspecializado(int iID) {
        for (LocalEspecializado local : this.m_lstLocaisEspecializados) {
            if (local.isIdentifiableAs(iID)) {
                return local;
            }
        }

        return null;
    }

    public CustoTransporte novoCustoTransporte() {
        return new CustoTransporte();
    }

    public CustoTransporte1 novoCustoTransporte1() {
        return new CustoTransporte1();
    }

    public boolean validaCustoTransporte(CustoTransporte transporte) {
        boolean bRet = false;
        if (transporte.valida()) {
            // Escrever aqui o código de validação

            //
            bRet = true;
        }
        return bRet;
    }

    public boolean validaCustoTransporte1(CustoTransporte1 transporte1) {
        boolean bRet = false;
        if (transporte1.valida()) {
            // Escrever aqui o código de validação

            //
            bRet = true;
        }
        return bRet;
    }

    public boolean registaCustoTransporte(CustoTransporte transporte) {
        if (this.validaCustoTransporte(transporte)) {
            return addCustoTransporte(transporte);
        }
        return false;
    }

    public boolean registaCustoTransporte1(CustoTransporte1 transporte1) {
        if (this.validaCustoTransporte1(transporte1)) {
            return addCustoTransporte1(transporte1);
        }
        return false;
    }

    public boolean addCustoTransporte(CustoTransporte transporte) {
        return m_lstCustoTransporte.add(transporte);
    }

    public boolean addCustoTransporte1(CustoTransporte1 transporte1) {
        return m_lstCustoTransporte1.add(transporte1);
    }

    public List<CustoTransporte> getListaCustoTransporte() {
        return this.m_lstCustoTransporte;
    }

    public List<CustoTransporte1> getListaCustoTransporte1() {
        return this.m_lstCustoTransporte1;
    }

    public void parametrosExemplo() {

        //COLABORADORES
        Colaborador col1 = new Colaborador();
        col1.setCodigo("C01");
        col1.setID(1150919);
        col1.setNome("Andreia Ferreira");
        col1.setDataNascimento("09/10/1997");
        col1.setEmail("1150919@isep.ipp.pt");
        this.m_lstColaboradores.add(col1);

        Colaborador col2 = new Colaborador();
        col2.setCodigo("C02");
        col2.setID(1150663);
        col2.setNome("Claudia Barbosa");
        col2.setDataNascimento("27/06/1997");
        col2.setEmail("1150663@isep.ipp.pt");
        this.m_lstColaboradores.add(col2);

        Colaborador col3 = new Colaborador();
        col3.setCodigo("C03");
        col3.setID(1150494);
        col3.setNome("Henrique Melo");
        col3.setDataNascimento("01/03/1995");
        col3.setEmail("1150494@isep.ipp.pt");
        this.m_lstColaboradores.add(col3);

        Colaborador col4 = new Colaborador();
        col4.setCodigo("C04");
        col4.setID(1150504);
        col4.setNome("Ines Pinto");
        col4.setDataNascimento("04/07/1995");
        col4.setEmail("1150504@isep.ipp.pt");
        this.m_lstColaboradores.add(col4);

        //LOCAIS DE PRODUCAO
        LocalProducao loc1 = new LocalProducao();
        loc1.setID(001);
        loc1.setDenominacao("Porto");
        loc1.setAbreviatura("PRT");
        loc1.setEndereco("Rua 1");
        this.m_lstLocaisProducao.add(loc1);

        LocalProducao loc2 = new LocalProducao();
        loc2.setID(002);
        loc2.setDenominacao("Xangai");
        loc2.setAbreviatura("XNG");
        loc2.setEndereco("Rua 2");
        this.m_lstLocaisProducao.add(loc2);

        LocalProducao loc3 = new LocalProducao();
        loc3.setID(003);
        loc3.setDenominacao("Tóquio");
        loc3.setAbreviatura("TQO");
        loc3.setEndereco("Rua 3");
        this.m_lstLocaisProducao.add(loc3);

        LocalProducao loc4 = new LocalProducao();
        loc4.setID(004);
        loc4.setDenominacao("Yokohama");
        loc4.setAbreviatura("YKH");
        loc4.setEndereco("Rua 4"); 
        this.m_lstLocaisProducao.add(loc4);

        LocalProducao loc5 = new LocalProducao();
        loc5.setID(005);
        loc5.setDenominacao("Amesterdao");
        loc5.setAbreviatura("AMS");
        loc5.setEndereco("Rua 5");
        this.m_lstLocaisProducao.add(loc5);

        LocalProducao loc6 = new LocalProducao();
        loc6.setID(006);
        loc6.setDenominacao("Moscovo");
        loc6.setAbreviatura("MOS");
        loc6.setEndereco("Rua 6");
        this.m_lstLocaisProducao.add(loc6);

        //LOCAIS DE DESTINO
        LocalDestino locD1 = new LocalDestino();
        locD1.setID(010);
        locD1.setDenominacao("Lisboa");
        locD1.setAbreviatura("LIS");
        locD1.setEndereco("Rua 10");
        this.m_lstLocaisDestino.add(locD1);

        LocalDestino locD2 = new LocalDestino();
        locD2.setID(011);
        locD2.setDenominacao("Joanesburgo");
        locD2.setAbreviatura("JNB");
        locD2.setEndereco("Rua 11");
        this.m_lstLocaisDestino.add(locD2);

        LocalDestino locD3 = new LocalDestino();
        locD3.setID(012);
        locD3.setDenominacao("Rio de Janeiro");
        locD3.setAbreviatura("RIO");
        locD3.setEndereco("Rua 12");
        this.m_lstLocaisDestino.add(locD3);

        //LOCAIS ESPECIALIZADOS
        LocalEspecializado locE1 = new LocalEspecializado();
        locE1.setID(020);
        locE1.setDenominacao("Riga");
        locE1.setAbreviatura("RIG");
        locE1.setEndereco("Rua 20");
        locE1.setCapacidadeTotal(300000);
        locE1.setCustoInstalacao(700000);
        this.m_lstLocaisEspecializados.add(locE1);

        LocalEspecializado locE2 = new LocalEspecializado();
        locE2.setID(021);
        locE2.setDenominacao("Timor");
        locE2.setAbreviatura("TIM");
        locE2.setEndereco("Rua 21");
        locE1.setCapacidadeTotal(550000);
        locE1.setCustoInstalacao(700000);
        this.m_lstLocaisEspecializados.add(locE2);

        LocalEspecializado locE3 = new LocalEspecializado();
        locE3.setID(022);
        locE3.setDenominacao("Bruxelas");
        locE3.setAbreviatura("BRX");
        locE3.setEndereco("Rua 22");
        locE1.setCapacidadeTotal(410000);
        locE1.setCustoInstalacao(700000);
        this.m_lstLocaisEspecializados.add(locE3);

        LocalEspecializado locE4 = new LocalEspecializado();
        locE4.setID(023);
        locE4.setDenominacao("Genebra");
        locE4.setAbreviatura("GNB");
        locE4.setEndereco("Rua 23");
        locE1.setCapacidadeTotal(830000);
        locE1.setCustoInstalacao(700000);
        this.m_lstLocaisEspecializados.add(locE4);

        LocalEspecializado locE5 = new LocalEspecializado();
        locE5.setID(024);
        locE5.setDenominacao("São Paulo");
        locE5.setAbreviatura("SPL");
        locE5.setEndereco("Rua 24");
        locE1.setCapacidadeTotal(260000);
        locE1.setCustoInstalacao(700000);
        this.m_lstLocaisEspecializados.add(locE5);

        LocalEspecializado locE6 = new LocalEspecializado();
        locE6.setID(025);
        locE6.setDenominacao("Nova Deli");
        locE6.setAbreviatura("NDL");
        locE6.setEndereco("Rua 25");
        locE1.setCapacidadeTotal(290000);
        locE1.setCustoInstalacao(700000);
        this.m_lstLocaisEspecializados.add(locE6);

        //MATÉRIAS-PRIMAS
        MateriaPrima mat1 = new MateriaPrima();
        mat1.setID("SOY");
        mat1.setCodigoAlfandegario("SMP1");
        mat1.setDescricaoBreve("Soja");
        mat1.setDescricaoCompleta("Soja Brasileira");
        this.m_lstMateriasPrimas.add(mat1);

        //PRODUTOS DERIVADOS
        ProdutoDerivado pd1 = new ProdutoDerivado();
        pd1.setID("SOY");
        pd1.setCodigoAlfandegario("SPD1");
        pd1.setDescricaoBreve("Soja");
        pd1.setDescricaoCompleta("Soja Importada");
        this.m_lstProdDerivados.add(pd1);

        ProdutoDerivado pd2 = new ProdutoDerivado();
        pd2.setID("MEAL");
        pd2.setCodigoAlfandegario("MPD1");
        pd2.setDescricaoBreve("Farelo");
        pd2.setDescricaoCompleta("Farelo de Soja");
        this.m_lstProdDerivados.add(pd2);

        ProdutoDerivado pd3 = new ProdutoDerivado();
        pd3.setID("OIL");
        pd3.setCodigoAlfandegario("OPD1");
        pd3.setDescricaoBreve("Oleo");
        pd3.setDescricaoCompleta("Oleo de Soja");
        this.m_lstProdDerivados.add(pd3);

        //Servicos
        Servico s1 = new Servico();
        s1.setCodigo("WARE");
        s1.setDescricao("Armazenar");
        s1.setFichaTecnica("FT1");
        this.m_lstServicos.add(s1);

        Servico s2 = new Servico();
        s2.setCodigo("SMASH");
        s2.setDescricao("Esmagar");
        s2.setFichaTecnica("FT2");
        this.m_lstServicos.add(s2);

        Servico s3 = new Servico();
        s3.setCodigo("PROC");
        s3.setDescricao("Processar");
        s3.setFichaTecnica("FT3");
        this.m_lstServicos.add(s3);
        
        //CUSTOS DE TRANSPORTE MATÉRIAS-PRIMAS
        CustoTransporte c1 = new CustoTransporte();
        c1.setLocalOrigem("PRT");
        c1.setLocalEspecializado("RIG");
        c1.setProduto("SOY");
        c1.setCusto(590);
        this.m_lstCustoTransporte.add(c1);
        
        CustoTransporte c2 = new CustoTransporte();
        c2.setLocalOrigem("PRT");
        c2.setLocalEspecializado("TIM");
        c2.setProduto("SOY");
        c2.setCusto(650);
        this.m_lstCustoTransporte.add(c2);
        
        CustoTransporte c3 = new CustoTransporte();
        c3.setLocalOrigem("PRT");
        c3.setLocalEspecializado("BRX");
        c3.setProduto("SOY");
        c3.setCusto(730);
        this.m_lstCustoTransporte.add(c3);
  
        CustoTransporte c4 = new CustoTransporte();
        c4.setLocalOrigem("PRT");
        c4.setLocalEspecializado("GNB");
        c4.setProduto("SOY");
        c4.setCusto(590);
        this.m_lstCustoTransporte.add(c4);
        
        CustoTransporte c5 = new CustoTransporte();
        c5.setLocalOrigem("PRT");
        c5.setLocalEspecializado("SPL");
        c5.setProduto("SOY");
        c5.setCusto(650);
        this.m_lstCustoTransporte.add(c5);

        CustoTransporte c6 = new CustoTransporte();
        c6.setLocalOrigem("PRT");
        c6.setLocalEspecializado("NDL");
        c6.setProduto("SOY");
        c6.setCusto(730);
        this.m_lstCustoTransporte.add(c6);

        CustoTransporte c7 = new CustoTransporte();
        c7.setLocalOrigem("XNG");
        c7.setLocalEspecializado("RIG");
        c7.setProduto("SOY");
        c7.setCusto(590);
        this.m_lstCustoTransporte.add(c7);
       
        CustoTransporte c8 = new CustoTransporte();
        c8.setLocalOrigem("XNG");
        c8.setLocalEspecializado("TIM");
        c8.setProduto("SOY");
        c8.setCusto(650);
        this.m_lstCustoTransporte.add(c8);
        
        CustoTransporte c9 = new CustoTransporte();
        c9.setLocalOrigem("XNG");
        c9.setLocalEspecializado("BRX");
        c9.setProduto("SOY");
        c9.setCusto(730);
        this.m_lstCustoTransporte.add(c9);
        
        CustoTransporte c10 = new CustoTransporte();
        c10.setLocalOrigem("XNG");
        c10.setLocalEspecializado("GNB");
        c10.setProduto("SOY");
        c10.setCusto(590);
        this.m_lstCustoTransporte.add(c10);

        CustoTransporte c11 = new CustoTransporte();
        c11.setLocalOrigem("XNG");
        c11.setLocalEspecializado("SPL");
        c11.setProduto("SOY");
        c11.setCusto(650);
        this.m_lstCustoTransporte.add(c11);

        CustoTransporte c12 = new CustoTransporte();
        c12.setLocalOrigem("XNG");
        c12.setLocalEspecializado("NDL");
        c12.setProduto("SOY");
        c12.setCusto(730);
        this.m_lstCustoTransporte.add(c12);

        CustoTransporte c13 = new CustoTransporte();
        c13.setLocalOrigem("TQO");
        c13.setLocalEspecializado("RIG");
        c13.setProduto("SOY");
        c13.setCusto(590);
        this.m_lstCustoTransporte.add(c13);

        
        CustoTransporte c14 = new CustoTransporte();
        c14.setLocalOrigem("TQO");
        c14.setLocalEspecializado("TIM");
        c14.setProduto("SOY");
        c14.setCusto(650);
        this.m_lstCustoTransporte.add(c14);

        CustoTransporte c15 = new CustoTransporte();
        c15.setLocalOrigem("TQO");
        c15.setLocalEspecializado("BRX");
        c15.setProduto("SOY");
        c15.setCusto(730);
        this.m_lstCustoTransporte.add(c15);

        CustoTransporte c16 = new CustoTransporte();
        c16.setLocalOrigem("TQO");
        c16.setLocalEspecializado("GNB");
        c16.setProduto("SOY");
        c16.setCusto(590);
        this.m_lstCustoTransporte.add(c16);
    
        CustoTransporte c17 = new CustoTransporte();
        c17.setLocalOrigem("TQO");
        c17.setLocalEspecializado("SPL");
        c17.setProduto("SOY");
        c17.setCusto(650);
        this.m_lstCustoTransporte.add(c17);

        CustoTransporte c18 = new CustoTransporte();
        c18.setLocalOrigem("TQO");
        c18.setLocalEspecializado("NDL");
        c18.setProduto("SOY");
        c18.setCusto(730);
        this.m_lstCustoTransporte.add(c18);
        
        CustoTransporte c19 = new CustoTransporte();
        c19.setLocalOrigem("YKH");
        c19.setLocalEspecializado("RIG");
        c19.setProduto("SOY");
        c19.setCusto(590);
        this.m_lstCustoTransporte.add(c19);
        
        CustoTransporte c20 = new CustoTransporte();
        c20.setLocalOrigem("YKH");
        c20.setLocalEspecializado("TIM");
        c20.setProduto("SOY");
        c20.setCusto(650);
        this.m_lstCustoTransporte.add(c20);

        
        CustoTransporte c21 = new CustoTransporte();
        c21.setLocalOrigem("YKH");
        c21.setLocalEspecializado("BRX");
        c21.setProduto("SOY");
        c21.setCusto(730);
        this.m_lstCustoTransporte.add(c21);

        
        CustoTransporte c22 = new CustoTransporte();
        c22.setLocalOrigem("YKH");
        c22.setLocalEspecializado("GNB");
        c22.setProduto("SOY");
        c22.setCusto(590);
        this.m_lstCustoTransporte.add(c22);

        
        CustoTransporte c23 = new CustoTransporte();
        c23.setLocalOrigem("YKH");
        c23.setLocalEspecializado("SPL");
        c23.setProduto("SOY");
        c23.setCusto(650);
        this.m_lstCustoTransporte.add(c23);

        
        CustoTransporte c24 = new CustoTransporte();
        c24.setLocalOrigem("YKH");
        c24.setLocalEspecializado("NDL");
        c24.setProduto("SOY");
        c24.setCusto(730);
        this.m_lstCustoTransporte.add(c24);

        
        CustoTransporte c25 = new CustoTransporte();
        c25.setLocalOrigem("AMS");
        c25.setLocalEspecializado("RIG");
        c25.setProduto("SOY");
        c25.setCusto(590);
        this.m_lstCustoTransporte.add(c25);
        
        CustoTransporte c26 = new CustoTransporte();
        c26.setLocalOrigem("AMS");
        c26.setLocalEspecializado("TIM");
        c26.setProduto("SOY");
        c26.setCusto(650);
        this.m_lstCustoTransporte.add(c26);
        
        CustoTransporte c27 = new CustoTransporte();
        c27.setLocalOrigem("AMS");
        c27.setLocalEspecializado("BRX");
        c27.setProduto("SOY");
        c27.setCusto(730);
        this.m_lstCustoTransporte.add(c27);
        
        CustoTransporte c28 = new CustoTransporte();
        c28.setLocalOrigem("AMS");
        c28.setLocalEspecializado("GNB");
        c28.setProduto("SOY");
        c28.setCusto(590);
        this.m_lstCustoTransporte.add(c28);
        
        CustoTransporte c29 = new CustoTransporte();
        c29.setLocalOrigem("AMS");
        c29.setLocalEspecializado("SPL");
        c29.setProduto("SOY");
        c29.setCusto(650);
        this.m_lstCustoTransporte.add(c29);
        
        CustoTransporte c30 = new CustoTransporte();
        c30.setLocalOrigem("AMS");
        c30.setLocalEspecializado("NDL");
        c30.setProduto("SOY");
        c30.setCusto(730);
        this.m_lstCustoTransporte.add(c30);
        
        CustoTransporte c31 = new CustoTransporte();
        c31.setLocalOrigem("MOS");
        c31.setLocalEspecializado("RIG");
        c31.setProduto("SOY");
        c31.setCusto(590);
        this.m_lstCustoTransporte.add(c31);
        
        CustoTransporte c32 = new CustoTransporte();
        c32.setLocalOrigem("MOS");
        c32.setLocalEspecializado("TIM");
        c32.setProduto("SOY");
        c32.setCusto(650);
        this.m_lstCustoTransporte.add(c32);
        
        CustoTransporte c33 = new CustoTransporte();
        c33.setLocalOrigem("MOS");
        c33.setLocalEspecializado("BRX");
        c33.setProduto("SOY");
        c33.setCusto(730);
        this.m_lstCustoTransporte.add(c33);
        
        CustoTransporte c34 = new CustoTransporte();
        c34.setLocalOrigem("MOS");
        c34.setLocalEspecializado("GNB");
        c34.setProduto("SOY");
        c34.setCusto(590);
        this.m_lstCustoTransporte.add(c34);

        
        CustoTransporte c35 = new CustoTransporte();
        c35.setLocalOrigem("MOS");
        c35.setLocalEspecializado("SPL");
        c35.setProduto("SOY");
        c35.setCusto(650);
        this.m_lstCustoTransporte.add(c35);
        
        CustoTransporte c36 = new CustoTransporte();
        c36.setLocalOrigem("MOS");
        c36.setLocalEspecializado("NDL");
        c36.setProduto("SOY");
        c36.setCusto(730);
        this.m_lstCustoTransporte.add(c36);
        
        //CUSTOS DE TRANSPORTE PRODUTOS DERIVADOS
        
        CustoTransporte1 ct1 = new CustoTransporte1();
        ct1.setLocalDestino("LIS");
        ct1.setLocalEspecializado("RIG");
        ct1.setProduto("SOY");
        ct1.setCusto(59);
        this.m_lstCustoTransporte1.add(ct1);
        
        CustoTransporte1 ct2 = new CustoTransporte1();
        ct2.setLocalDestino("LIS");
        ct2.setLocalEspecializado("RIG");
        ct2.setProduto("SOY");
        ct2.setCusto(65);
        this.m_lstCustoTransporte1.add(ct2);
        
        CustoTransporte1 ct3 = new CustoTransporte1();
        ct3.setLocalDestino("LIS");
        ct3.setLocalEspecializado("RIG");
        ct3.setProduto("SOY");
        ct3.setCusto(73);
        this.m_lstCustoTransporte1.add(ct3);
  
        CustoTransporte1 ct4 = new CustoTransporte1();
        ct4.setLocalDestino("JNB");
        ct4.setLocalEspecializado("RIG");
        ct4.setProduto("MEAL");
        ct4.setCusto(59);
        this.m_lstCustoTransporte1.add(ct4);
        
        CustoTransporte1 ct5 = new CustoTransporte1();
        ct5.setLocalDestino("JNB");
        ct5.setLocalEspecializado("RIG");
        ct5.setProduto("MEAL");
        ct5.setCusto(65);
        this.m_lstCustoTransporte1.add(ct5);

        CustoTransporte1 ct6 = new CustoTransporte1();
        ct6.setLocalDestino("JNB");
        ct6.setLocalEspecializado("RIG");
        ct6.setProduto("MEAL");
        ct6.setCusto(73);
        this.m_lstCustoTransporte1.add(ct6);

        CustoTransporte1 ct7 = new CustoTransporte1();
        ct7.setLocalDestino("RIO");
        ct7.setLocalEspecializado("RIG");
        ct7.setProduto("OIL");
        ct7.setCusto(59);
        this.m_lstCustoTransporte1.add(ct7);
       
        CustoTransporte1 ct8 = new CustoTransporte1();
        ct8.setLocalDestino("RIO");
        ct8.setLocalEspecializado("RIG");
        ct8.setProduto("OIL");
        ct8.setCusto(65);
        this.m_lstCustoTransporte1.add(ct8);
        
        CustoTransporte1 ct9 = new CustoTransporte1();
        ct9.setLocalDestino("RIO");
        ct9.setLocalEspecializado("RIG");
        ct9.setProduto("OIL");
        ct9.setCusto(73);
        this.m_lstCustoTransporte1.add(ct9);
        
        CustoTransporte1 ct10 = new CustoTransporte1();
        ct10.setLocalDestino("LIS");
        ct10.setLocalEspecializado("TIM");
        ct10.setProduto("SOY");
        ct10.setCusto(59);
        this.m_lstCustoTransporte1.add(ct10);

        CustoTransporte1 ct11 = new CustoTransporte1();
        ct11.setLocalDestino("LIS");
        ct11.setLocalEspecializado("TIM");
        ct11.setProduto("SOY");
        ct11.setCusto(65);
        this.m_lstCustoTransporte1.add(ct11);

        CustoTransporte1 ct12 = new CustoTransporte1();
        ct12.setLocalDestino("LIS");
        ct12.setLocalEspecializado("TIM");
        ct12.setProduto("SOY");
        ct12.setCusto(73);
        this.m_lstCustoTransporte1.add(ct12);

        CustoTransporte1 ct13 = new CustoTransporte1();
        ct13.setLocalDestino("JNB");
        ct13.setLocalEspecializado("TIM");
        ct13.setProduto("MEAL");
        ct13.setCusto(59);
        this.m_lstCustoTransporte1.add(ct13);

        
        CustoTransporte1 ct14 = new CustoTransporte1();
        ct14.setLocalDestino("JNB");
        ct14.setLocalEspecializado("TIM");
        ct14.setProduto("MEAL");
        ct14.setCusto(65);
        this.m_lstCustoTransporte1.add(ct14);

        CustoTransporte1 ct15 = new CustoTransporte1();
        ct15.setLocalDestino("JNB");
        ct15.setLocalEspecializado("TIM");
        ct15.setProduto("MEAL");
        ct15.setCusto(73);
        this.m_lstCustoTransporte1.add(ct15);

        CustoTransporte1 ct16 = new CustoTransporte1();
        ct16.setLocalDestino("RIO");
        ct16.setLocalEspecializado("TIM");
        ct16.setProduto("OIL");
        ct16.setCusto(59);
        this.m_lstCustoTransporte1.add(ct16);
    
        CustoTransporte1 ct17 = new CustoTransporte1();
        ct17.setLocalDestino("RIO");
        ct17.setLocalEspecializado("TIM");
        ct17.setProduto("OIL");
        ct17.setCusto(65);
        this.m_lstCustoTransporte1.add(ct17);

        CustoTransporte1 ct18 = new CustoTransporte1();
        ct18.setLocalDestino("RIO");
        ct18.setLocalEspecializado("TIM");
        ct18.setProduto("OIL");
        ct18.setCusto(73);
        this.m_lstCustoTransporte1.add(ct18);
        
        CustoTransporte1 ct19 = new CustoTransporte1();
        ct19.setLocalDestino("LIS");
        ct19.setLocalEspecializado("BRX");
        ct19.setProduto("SOY");
        ct19.setCusto(590);
        this.m_lstCustoTransporte1.add(ct19);
        
        CustoTransporte1 ct20 = new CustoTransporte1();
        ct20.setLocalDestino("LIS");
        ct20.setLocalEspecializado("BRX");
        ct20.setProduto("SOY");
        ct20.setCusto(65);
        this.m_lstCustoTransporte1.add(ct20);

        
        CustoTransporte1 ct21 = new CustoTransporte1();
        ct21.setLocalDestino("LIS");
        ct21.setLocalEspecializado("BRX");
        ct21.setProduto("SOY");
        ct21.setCusto(73);
        this.m_lstCustoTransporte1.add(ct21);

        
        CustoTransporte1 ct22 = new CustoTransporte1();
        ct22.setLocalDestino("JNB");
        ct22.setLocalEspecializado("BRX");
        ct22.setProduto("MEAL");
        ct22.setCusto(59);
        this.m_lstCustoTransporte1.add(ct22);

        
        CustoTransporte1 ct23 = new CustoTransporte1();
        ct23.setLocalDestino("JNB");
        ct23.setLocalEspecializado("BRX");
        ct23.setProduto("MEAL");
        ct23.setCusto(65);
        this.m_lstCustoTransporte1.add(ct23);

        
        CustoTransporte1 ct24 = new CustoTransporte1();
        ct24.setLocalDestino("JNB");
        ct24.setLocalEspecializado("BRX");
        ct24.setProduto("MEAL");
        ct24.setCusto(73);
        this.m_lstCustoTransporte1.add(ct24);

        
        CustoTransporte1 ct25 = new CustoTransporte1();
        ct25.setLocalDestino("RIO");
        ct25.setLocalEspecializado("BRX");
        ct25.setProduto("OIL");
        ct25.setCusto(59);
        this.m_lstCustoTransporte1.add(ct25);
        
        CustoTransporte1 ct26 = new CustoTransporte1();
        ct26.setLocalDestino("RIO");
        ct26.setLocalEspecializado("BRX");
        ct26.setProduto("OIL");
        ct26.setCusto(65);
        this.m_lstCustoTransporte1.add(ct26);
        
        CustoTransporte1 ct27 = new CustoTransporte1();
        ct27.setLocalDestino("RIO");
        ct27.setLocalEspecializado("BRX");
        ct27.setProduto("OIL");
        ct27.setCusto(73);
        this.m_lstCustoTransporte1.add(ct27);
        
        CustoTransporte1 ct28 = new CustoTransporte1();
        ct28.setLocalDestino("LIS");
        ct28.setLocalEspecializado("GNB");
        ct28.setProduto("SOY");
        ct28.setCusto(59);
        this.m_lstCustoTransporte1.add(ct28);
        
        CustoTransporte1 ct29 = new CustoTransporte1();
        ct29.setLocalDestino("LIS");
        ct29.setLocalEspecializado("GNB");
        ct29.setProduto("SOY");
        ct29.setCusto(65);
        this.m_lstCustoTransporte1.add(ct29);
        
        CustoTransporte1 ct30 = new CustoTransporte1();
        ct30.setLocalDestino("LIS");
        ct30.setLocalEspecializado("GNB");
        ct30.setProduto("SOY");
        ct30.setCusto(73);
        this.m_lstCustoTransporte1.add(ct30);
        
        CustoTransporte1 ct31 = new CustoTransporte1();
        ct31.setLocalDestino("JNB");
        ct31.setLocalEspecializado("GNB");
        ct31.setProduto("MEAL");
        ct31.setCusto(59);
        this.m_lstCustoTransporte1.add(ct31);
        
        CustoTransporte1 ct32 = new CustoTransporte1();
        ct32.setLocalDestino("JNB");
        ct32.setLocalEspecializado("GNB");
        ct32.setProduto("MEAL");
        ct32.setCusto(65);
        this.m_lstCustoTransporte1.add(ct32);
        
        CustoTransporte1 ct33 = new CustoTransporte1();
        ct33.setLocalDestino("JNB");
        ct33.setLocalEspecializado("GNB");
        ct33.setProduto("MEAL");
        ct33.setCusto(73);
        this.m_lstCustoTransporte1.add(ct33);
        
        CustoTransporte1 ct34 = new CustoTransporte1();
        ct34.setLocalDestino("RIO");
        ct34.setLocalEspecializado("GNB");
        ct34.setProduto("OIL");
        ct34.setCusto(59);
        this.m_lstCustoTransporte1.add(ct34);

        
        CustoTransporte1 ct35 = new CustoTransporte1();
        ct35.setLocalDestino("RIO");
        ct35.setLocalEspecializado("GNB");
        ct35.setProduto("OIL");
        ct35.setCusto(65);
        this.m_lstCustoTransporte1.add(ct35);
        
        CustoTransporte1 ct36 = new CustoTransporte1();
        ct36.setLocalDestino("RIO");
        ct36.setLocalEspecializado("GNB");
        ct36.setProduto("OIL");
        ct36.setCusto(73);
        this.m_lstCustoTransporte1.add(ct36);
        
        CustoTransporte1 ct37 = new CustoTransporte1();
        ct37.setLocalDestino("LIS");
        ct37.setLocalEspecializado("SPL");
        ct37.setProduto("SOY");
        ct37.setCusto(59);
        this.m_lstCustoTransporte1.add(ct37);
        
        CustoTransporte1 ct38 = new CustoTransporte1();
        ct38.setLocalDestino("LIS");
        ct38.setLocalEspecializado("SPL");
        ct38.setProduto("SOY");
        ct38.setCusto(65);
        this.m_lstCustoTransporte1.add(ct38);
        
        CustoTransporte1 ct39 = new CustoTransporte1();
        ct39.setLocalDestino("LIS");
        ct39.setLocalEspecializado("SPL");
        ct39.setProduto("SOY");
        ct39.setCusto(73);
        this.m_lstCustoTransporte1.add(ct39);
  
        CustoTransporte1 ct40 = new CustoTransporte1();
        ct40.setLocalDestino("JNB");
        ct40.setLocalEspecializado("SPL");
        ct40.setProduto("MEAL");
        ct40.setCusto(59);
        this.m_lstCustoTransporte1.add(ct40);
        
        CustoTransporte1 ct41 = new CustoTransporte1();
        ct41.setLocalDestino("JNB");
        ct41.setLocalEspecializado("SPL");
        ct41.setProduto("MEAL");
        ct41.setCusto(65);
        this.m_lstCustoTransporte1.add(ct41);

        CustoTransporte1 ct42 = new CustoTransporte1();
        ct42.setLocalDestino("JNB");
        ct42.setLocalEspecializado("SPL");
        ct42.setProduto("MEAL");
        ct42.setCusto(73);
        this.m_lstCustoTransporte1.add(ct42);

        CustoTransporte1 ct43 = new CustoTransporte1();
        ct43.setLocalDestino("RIO");
        ct43.setLocalEspecializado("SPL");
        ct43.setProduto("OIL");
        ct43.setCusto(59);
        this.m_lstCustoTransporte1.add(ct43);
       
        CustoTransporte1 ct44 = new CustoTransporte1();
        ct44.setLocalDestino("RIO");
        ct44.setLocalEspecializado("SPL");
        ct44.setProduto("OIL");
        ct44.setCusto(65);
        this.m_lstCustoTransporte1.add(ct44);
        
        CustoTransporte1 ct45 = new CustoTransporte1();
        ct45.setLocalDestino("RIO");
        ct45.setLocalEspecializado("SPL");
        ct45.setProduto("OIL");
        ct45.setCusto(73);
        this.m_lstCustoTransporte1.add(ct45);
        
        CustoTransporte1 ct46 = new CustoTransporte1();
        ct46.setLocalDestino("LIS");
        ct46.setLocalEspecializado("NDL");
        ct46.setProduto("SOY");
        ct46.setCusto(59);
        this.m_lstCustoTransporte1.add(ct46);

        CustoTransporte1 ct47 = new CustoTransporte1();
        ct47.setLocalDestino("LIS");
        ct47.setLocalEspecializado("NDL");
        ct47.setProduto("SOY");
        ct47.setCusto(65);
        this.m_lstCustoTransporte1.add(ct47);

        CustoTransporte1 ct48 = new CustoTransporte1();
        ct48.setLocalDestino("LIS");
        ct48.setLocalEspecializado("NDL");
        ct48.setProduto("SOY");
        ct48.setCusto(73);
        this.m_lstCustoTransporte1.add(ct48);

        CustoTransporte1 ct49 = new CustoTransporte1();
        ct49.setLocalDestino("JNB");
        ct49.setLocalEspecializado("NDL");
        ct49.setProduto("MEAL");
        ct49.setCusto(59);
        this.m_lstCustoTransporte1.add(ct49);

        
        CustoTransporte1 ct50 = new CustoTransporte1();
        ct50.setLocalDestino("JNB");
        ct50.setLocalEspecializado("NDL");
        ct50.setProduto("MEAL");
        ct50.setCusto(65);
        this.m_lstCustoTransporte1.add(ct50);

        CustoTransporte1 ct51 = new CustoTransporte1();
        ct51.setLocalDestino("JNB");
        ct51.setLocalEspecializado("NDL");
        ct51.setProduto("MEAL");
        ct51.setCusto(73);
        this.m_lstCustoTransporte1.add(ct51);

        CustoTransporte1 ct52 = new CustoTransporte1();
        ct52.setLocalDestino("RIO");
        ct52.setLocalEspecializado("NDL");
        ct52.setProduto("OIL");
        ct52.setCusto(59);
        this.m_lstCustoTransporte1.add(ct52);
    
        CustoTransporte1 ct53 = new CustoTransporte1();
        ct53.setLocalDestino("RIO");
        ct53.setLocalEspecializado("NDL");
        ct53.setProduto("OIL");
        ct53.setCusto(65);
        this.m_lstCustoTransporte1.add(ct53);

        CustoTransporte1 ct54 = new CustoTransporte1();
        ct54.setLocalDestino("RIO");
        ct54.setLocalEspecializado("NDL");
        ct54.setProduto("OIL");
        ct54.setCusto(73);
        this.m_lstCustoTransporte1.add(ct54);

    }

}
