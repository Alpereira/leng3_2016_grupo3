package utils;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Henrique Melo <1150494@isep.ipp.pt>
 */
public class Utils {

    static public String readLineFromConsole(String strPrompt) {
        try {
            System.out.println(strPrompt);

            InputStreamReader converter = new InputStreamReader(System.in);
            BufferedReader in = new BufferedReader(converter);

            return in.readLine();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    static public Date readDateFromConsole(String strPrompt) {
        do {
            try {
                String strDate = readLineFromConsole(strPrompt);

                SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");

                Date date = df.parse(strDate);

                return date;
            } catch (ParseException ex) {
                Logger.getLogger(Utils.class.getName()).log(Level.SEVERE, null, ex);
            }
        } while (true);
    }

    static public boolean confirma(String sMessage) {
        String strConfirma;
        do {
            strConfirma = Utils.readLineFromConsole("\n" + sMessage + "\n");
        } while (!strConfirma.equalsIgnoreCase("s") && !strConfirma.equalsIgnoreCase("n"));

        return strConfirma.equalsIgnoreCase("s");
    }

    static public List apresentaESeleciona(List list, String sHeader) {
        apresentaLista(list, sHeader);
        return selecionaObject(list);
    }

    static public void apresentaLista(List list, String sHeader) {
        System.out.println(sHeader);

        int index = 0;
        for (Object o : list) {
            index++;

            System.out.println(index + ". " + o.toString());
        }
        System.out.println("");
        System.out.println("0 - Cancelar");
    }

    static public String apresentaItem(List list, String sHeader, int t) {

        String y = null;
        String z = "";
        int p;
    
        for (Object o : list) {
            int n = 0;
            String a = o.toString();
         
            String b = a;
            if (t == 0) {
                p = a.indexOf(";");
                y = a.substring(0, p);
                y = y.concat(" ");
                z = z.concat(y);

            } else {
                if(t == 9){
                p = a.indexOf(";");
                y = a.substring(0, p);
                y = y.concat(" ");
                z = z.concat(y);
                a = a.substring(p+1,a.length());
                    System.out.println(a);
                p = a.indexOf(";");
                a=a.substring(p+1,a.length());
                p = a.indexOf(";");
                y = a.substring(0, p);
                y = y.concat(" ");
                z = z.concat(y) + " " + "\n";
                
                
                }else{
                    
                while (n < t) {
                    p = b.indexOf(";");
                    if (p != 0) {
                        n++;
                         b = b.substring(p + 1, b.length());
                     

                    }
                }
 
                y = a.substring(0, a.length()-b.length()-1);
                z = z.concat(y) + " "+ "\n";

            }
            }

        }
       
        z=z.replaceAll(";", " ");
                        System.out.println("yyyy" + z);
        return z ;
    }
    

    static public Object selecionaObject(List list) {
        String opcao;
        String x, t;
        int nOpcao = 1;
        do {
            opcao = Utils.readLineFromConsole("Introduza opção: ");
            nOpcao = new Integer(opcao);
        } while (nOpcao < 0 || nOpcao > list.size());

        if (nOpcao == 0) {
            return null;
        } else {
            x = list.get(nOpcao - 1).toString();
            t = x.substring(0, x.indexOf(";"));

            return t;
        }
    }

    public static int IntFromConsole(String strPrompt) {
        do {
            try {
                String strInt = readLineFromConsole(strPrompt);

                int iInt = Integer.parseInt(strInt);

                return iInt;
            } catch (NumberFormatException ex) {
                Logger.getLogger(Utils.class.getName()).log(Level.SEVERE, null, ex);
            }
        } while (true);
    }

    public static double DoubleFromConsole(String strPrompt) {
        do {
            try {
                String strDbl = readLineFromConsole(strPrompt);

                double dValor = Double.parseDouble(strDbl);

                return dValor;
            } catch (NumberFormatException ex) {
                Logger.getLogger(Utils.class.getName()).log(Level.SEVERE, null, ex);
            }
        } while (true);
    }
}
