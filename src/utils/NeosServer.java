package utils;

import java.io.File;
import org.neos.client.ResultReceiver;
import java.io.FileNotFoundException;
import java.util.Formatter;
import java.util.Vector;
import jobreceiver.JobReceiver;
import org.apache.xmlrpc.XmlRpcException;
import org.neos.client.FileUtils;
import org.neos.client.NeosJobXml;
import org.neos.client.NeosXmlRpcClient;

public class NeosServer {

    private static final String HOST = "neos-server.org";
    private static final String PORT = "3332";
    private static final String FILE_MOD = "AMPL.mod";
    private static final String FILE_DAT = "AMPL.dat";
    private static final String FILE_RUN = "AMPL.run";
    private static final String MODEL = "model";
    private static final String DATA = "data";
    private static final String COMMANDS = "commands";
    private static final String TIPO_PRB = "lp";
    private static final String TIPO_SOLVER = "CPLEX";
    private static final String TIPO_LANG = "AMPL";

    public static void estabelecerLigacao(String fich_out) throws FileNotFoundException {
        /* create NeosXmlRpcClient object with server information */
        NeosXmlRpcClient client = new NeosXmlRpcClient(HOST, PORT);

        NeosJobXml exJob = new NeosJobXml(TIPO_PRB, TIPO_SOLVER, TIPO_LANG);

        FileUtils fileUtils = FileUtils.getInstance(FileUtils.APPLICATION_MODE);

//        exemplo
        String example = fileUtils.readFile(FILE_MOD);
        exJob.addParam(MODEL, example);

//        exemplo
        example = fileUtils.readFile(FILE_DAT);
        exJob.addParam(DATA, example);

//        exemplo
        example = fileUtils.readFile(FILE_RUN);
        /* add contents of string example to model field of job XML */
        exJob.addParam(COMMANDS, example);

        /* convert job XML to string and add it to the parameter vector */
        Vector params = new Vector();
        String jobString = exJob.toXMLString();
        params.add(jobString);

        Integer currentJob;
        String currentPassword;

        try {
            client.connect();
            /* invoke submitJob method on server and wait for response for 5000 ms */
            // params é do tipo Vector...
            Object[] results = (Object[]) client.execute("submitJob", params, 5000000);

            /* get returned values of job number and job password */
            currentJob = (Integer) results[0];
            currentPassword = (String) results[1];

            /* initialize receiver and start output monitoring */
            JobReceiver jobReceiver = new JobReceiver();
            ResultReceiver receiver = new ResultReceiver(client, jobReceiver,
                    currentJob, currentPassword);
            receiver.start();

            try (Formatter out = new Formatter(new File("Results.txt"))) {
                out.format("%s", receiver.getResult());
            }

        } catch (XmlRpcException e) {
            System.out.println("Error submitting job :" + e.getMessage());
            return;
        }
    }

}
