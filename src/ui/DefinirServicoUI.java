package ui;

import controller.DefinirServicoController;
import java.util.ArrayList;
import java.util.List;
import registos.registoGeral;
import model.LocalProducao;
import model.MateriaPrima;
import model.Produto;
import model.ProdutoDerivado;
import utils.Utils;

/**
 *
 * @author Henrique Melo <1150494@isep.ipp.pt>
 */
public class DefinirServicoUI {

    public registoGeral rg;
    private final DefinirServicoController m_controller;

    public DefinirServicoUI(registoGeral Rg) {
        this.rg = Rg;
        m_controller = new DefinirServicoController(Rg);
    }


    public void run() throws ClassCastException{
        System.out.println("\nNovo Serviço:");
        m_controller.novoServico();

        introduzDados();
        List<ProdutoDerivado> lstProds = m_controller.getProdutosDerivados();
        List<MateriaPrima> lstMP = m_controller.getMateriaPrima();
        if (lstMP.size() > 0) {
            boolean bNew = true;
            do {
                Object mp = Utils.apresentaESeleciona(lstMP, "Selecione a Matéria-Prima:");
                if (mp != null) {
                    Object pr = Utils.apresentaESeleciona(lstProds, "Selecione o Produto Resultante:");
                    if (pr != null) {
                        double racio = Utils.DoubleFromConsole("Indique o rácio:");

                        m_controller.setMateriaPrima(mp);
                        m_controller.setProdutoResultante(pr);
                        m_controller.setRacio(racio);
                    }
                }

                bNew = Utils.confirma("Deseja inserir outra aplicação do serviço (S/N?)");
            } while (bNew);

            int a = 0;
            boolean continuar = false;
            System.out.println("\nAdicionar serviço a local:");
            List<LocalProducao> m_lstLocaisProducao = m_controller.getLocaisProducao();
            do {
                Object lp = Utils.apresentaESeleciona(m_lstLocaisProducao, "Locais de Produção:");
                double custo = Utils.DoubleFromConsole("\nInsira o Custo do Serviço:");
                int capacidade = Utils.IntFromConsole("Insira a Capacidade Nominal:");
                m_controller.addLocal((LocalProducao) lp, custo, capacidade);
                a++;
                if (a > 0) {
                    continuar = !Utils.confirma("Adicionar outro Local? (S/N)");
                }

            } while (continuar == false);

            System.out.println("\nAdicionar serviço a possivel futuro local:");
            do {
                Object lp = Utils.apresentaESeleciona(m_lstLocaisProducao, "Locais de Produção:");
                double custo = Utils.DoubleFromConsole("\nInsira o Custo do serviço:");
                m_controller.addLocalFuturo((LocalProducao) lp, custo);

                continuar = !Utils.confirma("Adicionar outro Local Futuro? (S/N)");

            } while (continuar == false);
        } else {
            System.out.println("Não é possivel especificar Aplicações porque não existem Matérias-Primas definidas.");
        }

        apresentaDados();

        if (Utils.confirma("Confirma os dados do Serviço? (S/N)")) {
            if (m_controller.registaServico()) {
                System.out.println("Serviço registado.");
            } else {
                System.out.println("Serviço não registado.");
            }
        }
    }

    private void introduzDados() {
        String sCod = Utils.readLineFromConsole("Introduza o Código:");
        String sDescricao = Utils.readLineFromConsole("Introduza a Descrição: ");
        String sFxTec = Utils.readLineFromConsole("Introduza a Ficha Técnica: ");

        m_controller.setDados(sCod, sDescricao, sFxTec);
    }

    private void apresentaDados() {
        System.out.println("\nServiço:\n" + m_controller.getServicoAsString());
        System.out.println("\nLocais que Disponibilizam o Serviço Pretendido:\nLocal:");
        m_controller.locaisComServiçoToString();
        System.out.println("\nLocais que no Futuro Poderiam Disponibilizar o Serviço:");
        m_controller.locaisComServiçoFuturoToString();
    }

}
