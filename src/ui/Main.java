package ui;

import registos.registoGeral;

/**
 *
 * @author Henrique Melo <1150494@isep.ipp.pt>
 */
public class Main {

    public static void main(String[] args) {
        try {
            registoGeral rGer = new registoGeral();

            MenuUI uiMenu = new MenuUI(rGer);

            uiMenu.run();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
