package ui;

import controller.EspecificarLocalProducaoController;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Formatter;
import java.util.List;
import java.util.Locale;
import model.Colaborador;
import model.LocalProducao;
import registos.registoGeral;
import model.MateriaPrima;
import utils.Utils;

/**
 *
 * @author Henrique Melo <1150494@isep.ipp.pt>
 */
public class EspecificarLocalProducaoUI {

    public registoGeral rg;
    private final EspecificarLocalProducaoController m_controller;
    private static ArrayList<LocalProducao> lp = new ArrayList<>();


    public EspecificarLocalProducaoUI(registoGeral Rg) {
        this.rg = Rg;
        m_controller = new EspecificarLocalProducaoController(Rg);
    }

    public void run() throws IOException {
        
        System.out.println("\nNovo Local de Produção:");
        m_controller.novoLocalProducao();

        introduzDados();        
        apresentaDados();

        if (Utils.confirma("Confirma os dados do Local de Produção? (S/N)")) {
            if (m_controller.registaLocalProducao()) {
                System.out.println("Local de Produção registado.");
            } else {
                System.out.println("Local de Produção não registado.");
            }
        }
    }

    private void introduzDados() {
        List<Colaborador> c = m_controller.getColaboradores();
        List<MateriaPrima> mp = m_controller.getMateriasPrimas();
        
        int id = Utils.IntFromConsole("Introduza o Código:");
        String denominacao = Utils.readLineFromConsole("Introduza a Denominação: ");
        String abreviatura = Utils.readLineFromConsole("Introduza a Abreviatura: ");
        String endereco = Utils.readLineFromConsole("Introduza o Endereço: ");
        String coordGPS = Utils.readLineFromConsole("Introduza as Coordenadas GPS: ");
        Object col = Utils.apresentaESeleciona(c, "Escolha um Colaborador da Lista:");
        Object m = Utils.apresentaESeleciona(mp, "Escolha uma Matéria-Prima:");


        m_controller.setDados(id, denominacao, abreviatura, endereco, coordGPS,col,m);

    }
   public static void gravar(ArrayList<LocalProducao> lp) throws IOException {

        Formatter fout = new Formatter(new File("LocaisProducao1.txt"));
     for (LocalProducao p : lp) {
         
             fout.format(Locale.ENGLISH,"%s;", p.toString());
        
        fout.flush();
        fout.close();
        System.out.println("Escrita concluida...");
     
    }
   }
    private void apresentaDados() {
        System.out.println("\nLocal de Produção:\n" + m_controller.getLocalProducaoAsString());
    }

    }


