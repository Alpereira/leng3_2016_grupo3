package ui;

import controller.EspecificarProdutoDerivadoController;
import registos.registoGeral;
import utils.Utils;

/**
 *
 * @author Henrique Melo <1150494@isep.ipp.pt>
 */
public class EspecificarProdutoDerivadoUI {

    public registoGeral rg;
    private final EspecificarProdutoDerivadoController m_controller;

    public EspecificarProdutoDerivadoUI(registoGeral Rg) {
        this.rg = Rg;
        m_controller = new EspecificarProdutoDerivadoController(Rg);
    }

    public void run() {
        System.out.println("\nNovo Produto Derivado:");
        m_controller.novoProdutoDerivado();

        introduzDados();

        apresentaDados();

        if (Utils.confirma("Confirma os dados do Produto Derivado? (S/N)")) {
            if (m_controller.registaProdutoDerivado()) {
                System.out.println("Produto Derivado registado.");
            } else {
                System.out.println("Produto Derivado não registado.");
            }
        }
    }

    private void introduzDados() {
        String id = Utils.readLineFromConsole("Introduza o ID:");
        String descricaoBreve = Utils.readLineFromConsole("Introduza Descrição Breve: ");
        String descricaoCompleta = Utils.readLineFromConsole("Introduza Descrição Completa: ");
        String codAlfandegario = Utils.readLineFromConsole("Código Alfandegário: ");

        m_controller.setDados(id, descricaoBreve, descricaoCompleta, codAlfandegario);
    }

    private void apresentaDados() {
        System.out.println("\nProduto Derivado:\n" + m_controller.getProdutoDerivadoAsString());
    }
}
