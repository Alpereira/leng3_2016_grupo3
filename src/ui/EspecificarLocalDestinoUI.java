package ui;

import controller.EspecificarLocalDestinoController;
import java.util.List;
import registos.registoGeral;
import model.Produto;
import model.LocalP;
import utils.Utils;

/**
 *
 * @author Henrique Melo <1150494@isep.ipp.pt>
 */
public class EspecificarLocalDestinoUI {

    public registoGeral rg;
    private final EspecificarLocalDestinoController m_controller;

    public EspecificarLocalDestinoUI(registoGeral Rg) {
        this.rg = Rg;
        m_controller = new EspecificarLocalDestinoController(Rg);
    }

    public void run() {
        System.out.println("\nNovo Local Destino:");
        m_controller.novoLocalDestino();

        introduzDados();

        List<Produto> lst_produtos = m_controller.getProdutos();

        boolean continuar;
        do {
            Object produto = Utils.apresentaESeleciona(lst_produtos, "Escolha o produto:");
            double custo = Utils.DoubleFromConsole("\nInsira o Custo do produto escolhido:");
            int procura = Utils.IntFromConsole("Insira a Procura Prevista para o Produto:");
            LocalP produtoEmLocal = new LocalP(produto, custo, procura);
            m_controller.addProduto(produtoEmLocal);

            continuar = !Utils.confirma("Adicionar outro produto?");
        } while (continuar == false);

        apresentaDados();

        if (Utils.confirma("Confirma os dados do Local de Produção? (S/N)")) {
            if (m_controller.registaLocalDestino()) {
                System.out.println("Local de Destino registado.");
            } else {
                System.out.println("Local de Destino não registado.");
            }
        }
    }

    private void introduzDados() {
        int id = Utils.IntFromConsole("Introduza o id:");
        String denominacao = Utils.readLineFromConsole("Introduza a Denominação: ");
        String abreviatura = Utils.readLineFromConsole("Introduza a Abreviatura: ");
        String endereco = Utils.readLineFromConsole("Introduza o Endereço: ");
        String coordGPS = Utils.readLineFromConsole("Introduza as Coordenadas GPS: ");
        double custo = Utils.DoubleFromConsole("Introduza o Custo de Instalação: ");
        int capacidade = Utils.IntFromConsole("Insira a Capacidade Total do Local: ");

        m_controller.setDados(id, denominacao, abreviatura, endereco, coordGPS, custo, capacidade);
    }

    private void apresentaDados() {
        System.out.println("\nLocal de Produção:\n" + m_controller.getLocalDestinoAsString());
    }
}
