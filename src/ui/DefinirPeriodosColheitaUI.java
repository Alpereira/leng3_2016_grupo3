package ui;

import controller.DefinirPeriodosColheitaController;
import java.util.Date;
import java.util.List;
import registos.registoGeral;
import model.MateriaPrima;
import utils.Utils;

/**
 *
 * @author Henrique Melo <1150494@isep.ipp.pt>
 */
public class DefinirPeriodosColheitaUI {

    public registoGeral rg;
    private final DefinirPeriodosColheitaController m_controller;

    public DefinirPeriodosColheitaUI(registoGeral Rg) {
        this.rg = Rg;
        m_controller = new DefinirPeriodosColheitaController(Rg);
    }

    public void run() {
        List<MateriaPrima> lmp = m_controller.getListaMateriasPrimas();

        if (lmp.size() > 0) {
            MateriaPrima mp = (MateriaPrima) Utils.apresentaESeleciona(lmp, "Selecione a Matéria-Prima:");

            if (mp != null) {
                m_controller.setMateriaPrima(mp);
                boolean bNew = false;
                do {
                    Date dtIni = Utils.readDateFromConsole("Insira a Data de Inicio no Formato DD-MM-YY:");
                    Date dtFim = Utils.readDateFromConsole("Data de Fim no Formato DD-MM-YY:");

                    if (m_controller.addPeriodoColheita(dtIni, dtFim)) {
                        System.out.println("Periodo de Colheita registado.");
                    } else {
                        System.out.println("Periodo de Colheita não registado.");
                    }

                    bNew = Utils.confirma("Inserir outro periodo de colheita para a mesma matéria-prima (S/N)?");

                } while (bNew);
            }
        } else {
            System.out.println("Não existem matérias-primas definidas.");
        }
    }

}
