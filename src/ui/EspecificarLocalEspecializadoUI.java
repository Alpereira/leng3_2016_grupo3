package ui;

import controller.EspecificarLocalEspecializadoController;
import java.util.List;
import registos.registoGeral;
import model.Servico;
import model.Disponibilizacao;
import utils.Utils;

/**
 *
 * @author Henrique Melo <1150494@isep.ipp.pt>
 */
public class EspecificarLocalEspecializadoUI {

    public registoGeral rg;
    private final EspecificarLocalEspecializadoController m_controller;

    public EspecificarLocalEspecializadoUI(registoGeral Rg) {
        this.rg = Rg;
        m_controller = new EspecificarLocalEspecializadoController(Rg);
    }

    public void run() {
        System.out.println("\nNovo Local Especializado:");
        m_controller.novoLocalEspecializado();

        introduzDados();

        List<Servico> lst_servicos = m_controller.getServicos();
        boolean continuar;
        do {
            Object servico = Utils.apresentaESeleciona(lst_servicos, "Escolha o serviço:");
            double custo = Utils.DoubleFromConsole("\nInsira o Custo do serviço:");
            int capacidade = Utils.IntFromConsole("Insira a Capacidade nominal:");
            Disponibilizacao servicoEmLocal = new Disponibilizacao(servico, custo, capacidade);
            m_controller.addServico(servicoEmLocal);

            continuar = !Utils.confirma("Adicionar outro servico? (S/N)");
        } while (continuar == false);

        apresentaDados();

        if (Utils.confirma("Confirma os dados do Local de Produção? (S/N)")) {
            if (m_controller.registaLocalEspecializado()) {
                System.out.println("Local de Produção registado.");
            } else {
                System.out.println("Local de Produção não registado.");
            }
        }
    }

    private void introduzDados() {
        int ID = Utils.IntFromConsole("Introduza o ID:");
        String sDenominacao = Utils.readLineFromConsole("Introduza Denominação: ");
        String sAbreviatura = Utils.readLineFromConsole("Introduza Abreviatura: ");
        String sEndereco = Utils.readLineFromConsole("Introduza Endereço: ");
        String sCoordGPS = Utils.readLineFromConsole("Introduza Coordenadas GPS: ");
        double dCusto = Utils.DoubleFromConsole("Introduza Custo de Instalação: ");
        int iCapacidade = Utils.IntFromConsole("Insira a Capacidade Total: ");

        m_controller.setDados(ID, sDenominacao, sAbreviatura, sEndereco, sCoordGPS, dCusto, iCapacidade);
    }

    private void apresentaDados() {
        System.out.println("\nLocal Especializado:\n" + m_controller.getLocalEspecializadoAsString());
    }
}
