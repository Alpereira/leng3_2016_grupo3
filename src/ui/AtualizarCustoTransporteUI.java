package ui;

import controller.AtualizarCustoTransporte1Controller;
import controller.AtualizarCustoTransporteController;

import java.util.Date;
import java.util.List;
import registos.registoGeral;
import model.*;
import utils.Utils;

/**
 *
 * @author Henrique Melo <1150494@isep.ipp.pt>
 */
public class AtualizarCustoTransporteUI {

    public registoGeral rg;
    private final AtualizarCustoTransporteController m_controller;
    private final AtualizarCustoTransporte1Controller m_controller1;

    public AtualizarCustoTransporteUI(registoGeral Rg) {
        this.rg = Rg;
        m_controller = new AtualizarCustoTransporteController(Rg);
        m_controller1 = new AtualizarCustoTransporte1Controller(Rg);
    }

    public void run() {

        List<LocalProducao> lProducao = m_controller.getLocaisProducao();
        List<LocalDestino> lDestino = m_controller1.getLocaisDestino();
        List<LocalEspecializado> lEspec = m_controller.getLocaisEspecializados();
        List<MateriaPrima> mp = m_controller.getMateria();
        List<ProdutoDerivado> pd = m_controller1.getDerivados();

        int escolha = Utils.IntFromConsole("Deseja introduzir custo para:\n1) Matéria-Prima\n2) Produto Derivado");
        if (escolha == 1) {
            System.out.println("\nAtualizar Custo Transporte:");
            m_controller.novoCustoTransporte();

            Object locOrigem = Utils.apresentaESeleciona(lProducao, "Escolha o Local de Origem:");
            Object locEspec = Utils.apresentaESeleciona(lEspec, "Escolha o Local Especializado:");
            Object mat = Utils.apresentaESeleciona(mp, "Introduza a Materia-Prima:");
            double custo = Utils.DoubleFromConsole("Introduza o Custo de transporte:");
            Date data = Utils.readDateFromConsole("Introduza a Data de entrega no formato DD-MM-YY");
            m_controller.setDados(locOrigem, locEspec, mat, custo, data);

            System.out.println("\nCusto de Transporte:\n" + m_controller.getCustoTransporteAsString());

            if (Utils.confirma("Confirma os dados do Custo de Transporte? (S/N)")) {
                if (m_controller.registaCustoTransporte()) {
                    System.out.println("Custo de transporte registado.");
                    
                } else {
                    System.out.println("Custo de Transporte não registado.");
                }
            }

        } else {

            System.out.println("\nAtualizar Custo Transporte:");
            m_controller1.novoCustoTransporte1();

            Object locEspe = Utils.apresentaESeleciona(lEspec, "Escolha o Local Especializado:");
            Object locDestino = Utils.apresentaESeleciona(lDestino, "Escolha o Local de Destino:");
            Object prod = Utils.apresentaESeleciona(pd, "Introduza o Produto Derivado:");
            double custo = Utils.DoubleFromConsole("Introduza o Custo de transporte:");
            Date data = Utils.readDateFromConsole("Introduza a Data de entrega no formato DD-MM-YY");
            m_controller1.setDados1(locEspe, locDestino, prod, custo, data);

            System.out.println("\nCusto de Transporte:\n" + m_controller1.getCustoTransporte1AsString());

            if (Utils.confirma("Confirma os dados do Custo de Transporte? (S/N)")) {
                if (m_controller1.registaCustoTransporte1()) {
                    System.out.println("Custo de transporte registado.");
                } else {
                    System.out.println("Custo de Transporte não registado.");
                }
            }
        }

    }
}
