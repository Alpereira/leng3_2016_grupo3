package ui;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Formatter;
import java.util.Scanner;
import registos.registoGeral;
import utils.Utils;
import model.*;

/**
 *
 * @author Henrique Melo <1150494@isep.ipp.pt>
 */
public class MenuUI {

    public registoGeral rg;
    private String opcao;
    public Object m_controller;
    static Scanner in = new Scanner(System.in).useDelimiter("\n");
    static Formatter out = new Formatter(System.out);
    private static ArrayList<LocalProducao> lp = new ArrayList<>();
    private static ArrayList<LocalDestino> ld = new ArrayList<>();
    private static ArrayList<LocalEspecializado> le = new ArrayList<>();
    private static ArrayList<MateriaPrima> mp = new ArrayList<>();
    private static ArrayList<ProdutoDerivado> pd = new ArrayList<>();
    private static ArrayList<PeriodoColheita> pc = new ArrayList<>();
    private static ArrayList<Servico> ser = new ArrayList<>();
    private static ArrayList<Previsao> prev = new ArrayList<>();
    private static ArrayList<CustoTransporte> ct = new ArrayList<>();
    private static ArrayList<CustoTransporte1> ct1 = new ArrayList<>();
    private static ArrayList<PrecoVenda> pv= new ArrayList<>();
    

    public MenuUI(registoGeral r) {
        this.rg = r;
    }

    public void run() throws IOException, FileNotFoundException, ClassNotFoundException {
        do {

            System.out.println("\n\n");
            System.out.println("1. Especificar Local de Produção");
            System.out.println("2. Especificar Matéria-Prima");
            System.out.println("3. Definir Produto Derivado");
            System.out.println("4. Definir Periodos de Colheita");
            System.out.println("5. Definir Serviço");
            System.out.println("6. Efetuar Previsão de Produção");
            System.out.println("7. Especificar Local Especializado ");
            System.out.println("8. Especificar Local de Destino");
            System.out.println("9. Atualizar Custo de Transporte");
            System.out.println("10. Estipular Preço de Venda em Local");
            System.out.println("11. Atualizar Informação de Colaboradores");
            System.out.println("12. Optimizar Custo de Transporte");

            System.out.println("0. Sair");

            opcao = Utils.readLineFromConsole("Introduza opção: ");

            if (opcao.equals("1")) {
                EspecificarLocalProducaoUI ui = new EspecificarLocalProducaoUI(rg);
                ui.run();
                LocalProducao.gravar(lp);
                LocalProducao.ler();
            }
            if (opcao.equals("2")) {
                EspecificarMateriaPrimaUI ui = new EspecificarMateriaPrimaUI(rg);
                ui.run();
                Produto.gravar(mp);
            }
            if (opcao.equals("3")) {
                EspecificarProdutoDerivadoUI ui = new EspecificarProdutoDerivadoUI(rg);
                ui.run();
                Produto.gravar2(pd);
            }

            if (opcao.equals("4")) {
                DefinirPeriodosColheitaUI ui = new DefinirPeriodosColheitaUI(rg);
                ui.run();
                PeriodoColheita.gravar(pc);
            }

            if (opcao.equals("5")) {
                DefinirServicoUI ui = new DefinirServicoUI(rg);
                ui.run();
                Servico.gravar(ser);
            }

            if (opcao.equals("6")) {
                EfetuarPrevisaoProducaoUI ui = new EfetuarPrevisaoProducaoUI(rg);
                ui.run();
                Previsao.gravar(prev);
            }

            if (opcao.equals("7")) {
                EspecificarLocalEspecializadoUI ui = new EspecificarLocalEspecializadoUI(rg);
                ui.run();
                LocalEspecializado.gravar(le);
            }

            if (opcao.equals("8")) {
                EspecificarLocalDestinoUI ui = new EspecificarLocalDestinoUI(rg);
                ui.run();
                LocalDestino.gravar(ld);
            }

            if (opcao.equals("9")) {
                AtualizarCustoTransporteUI ui = new AtualizarCustoTransporteUI(rg);
                ui.run();
                CustoTransporte.gravar(ct);
                CustoTransporte1.gravar(ct1);

                
            }

            if (opcao.equals("10")) {
                EstipularPrecoVendaEmLocalUI ui = new EstipularPrecoVendaEmLocalUI(rg);
                ui.run();
                PrecoVenda.gravar(pv);
            }

            if (opcao.equals("11")) {
                String file = Utils.readLineFromConsole("Ficheiro Encontrado: colaboradores.csv\nDeseja ler este ficheiro? (S/N)");
                char f = file.charAt(0);
                if (f == 's' || f == 'S') {
                    System.out.println("\n\n\nA ler ficheiro...\n\n");
                    rg.lerColaboradores();
                } else {
                    System.out.println("Concluido.");
                }

            }

            if (opcao.equals("12")) {
                OptimizarCustoTransporteUI ui = new OptimizarCustoTransporteUI(rg);
                ui.run();
            }

        } while (!opcao.equals("0"));
    }
}
