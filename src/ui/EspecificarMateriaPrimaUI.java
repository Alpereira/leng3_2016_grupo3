package ui;

import controller.EspecificarMateriaPrimaController;
import registos.registoGeral;
import utils.Utils;

/**
 *
 * @author Henrique Melo <1150494@isep.ipp.pt>
 */
public class EspecificarMateriaPrimaUI {

    public registoGeral rg;
    private final EspecificarMateriaPrimaController m_controller;

    public EspecificarMateriaPrimaUI(registoGeral Rg) {
        this.rg = Rg;
        m_controller = new EspecificarMateriaPrimaController(Rg);
    }

    public void run() {
        System.out.println("\nNova Matéria-Prima:");
        m_controller.novaMateriaPrima();

        introduzDados();

        apresentaDados();

        if (Utils.confirma("Confirma os dados da Matéria-Prima? (S/N)")) {
            if (m_controller.registaMateriaPrima()) {
                System.out.println("Matéria-Prima registada.");
            } else {
                System.out.println("Matéria-Prima não registada.");
            }
        }
    }

    private void introduzDados() {
        String sID = Utils.readLineFromConsole("Introduza Abreviatura do nome:");
        String sDescBreve = Utils.readLineFromConsole("Introduza Descrição Breve: ");
        String sDescCompleta = Utils.readLineFromConsole("Introduza Descrição Completa: ");
        String sCodAlfa = Utils.readLineFromConsole("Código Alfandegário: ");

        m_controller.setDados(sID, sDescBreve, sDescCompleta, sCodAlfa);
    }

    private void apresentaDados() {
        System.out.println("\nMatéria-Prima:\n" + m_controller.getMateriaPrimaAsString());
    }
}
