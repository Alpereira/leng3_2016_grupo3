package ui;

import controller.EstipularPrecoVendaEmLocalController;
import java.util.List;
import model.LocalDestino;
import model.ProdutoDerivado;
import registos.registoGeral;
import utils.Utils;

/**
 *
 * @author Henrique Melo <1150494@isep.ipp.pt>
 */
public class EstipularPrecoVendaEmLocalUI {

    public registoGeral rg;
    private final EstipularPrecoVendaEmLocalController m_controller;

    public EstipularPrecoVendaEmLocalUI(registoGeral Rg) {

        this.rg = Rg;
        m_controller = new EstipularPrecoVendaEmLocalController(Rg);
    }

    public void run() {
        System.out.println("\nPreço de Venda de Local:");
        m_controller.novoPrecoVenda();

        List<ProdutoDerivado> pd = this.rg.getDerivados();
        List<LocalDestino> locaisDestino = rg.getListaLocaisDestino();

        Object destino = Utils.apresentaESeleciona(locaisDestino, "Escolha o Local de Origem:");
        String ano = Utils.readLineFromConsole("Introduza o Ano Civil:");
        Object produto = Utils.apresentaESeleciona(pd, "Escolha o Produto Derivado:");
        String preco = Utils.readLineFromConsole("Introduza o Preço de Venda:");
        String qnt = Utils.readLineFromConsole("Introduza a Quantidade de Procura Prevista:");

        m_controller.setDados(destino, ano, produto, preco, qnt);

        apresentaDados();

        if (Utils.confirma("Confirma os dados Introduzidos? (S/N)")) {
            if (m_controller.registaPrecoVenda()) {
                System.out.println("Preço de Venda registado.");
            } else {
                System.out.println("Preço de Venda não registado.");
            }
        }
    }

    private void apresentaDados() {
        System.out.println("\nPreço de Venda:\n" + m_controller.getPrecoVendaAsString());
    }
}
