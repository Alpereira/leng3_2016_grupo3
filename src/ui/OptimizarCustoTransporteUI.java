package ui;

import controller.*;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import registos.registoGeral;
import model.LocalProducao;
import model.MateriaPrima;
import utils.Utils;
import java.util.*;
import model.CustoTransporte;
import model.CustoTransporte1;
import model.LocalDestino;
import model.LocalEspecializado;
import model.PrecoVenda;
import model.ProdutoDerivado;
import model.Servico;
import utils.NeosServer;

/**
 *
 * @author Henrique Melo <1150494@isep.ipp.pt>
 */
public class OptimizarCustoTransporteUI {

    public LocalDestino m_oLocalDestino;
    public LocalEspecializado m_oLocalEspecializado;
    public MateriaPrima m_oMatPrima;
    public LocalProducao m_oLocalProducao;
    public registoGeral rg;
    private final OptimizarCustoTransporteController m_controller;
    public ArrayList<LocalProducao> lp;

    public OptimizarCustoTransporteUI(registoGeral Rg) {
        this.rg = Rg;
        m_controller = new OptimizarCustoTransporteController(Rg);
    }

    public void run() throws FileNotFoundException, UnsupportedEncodingException {
        
        List<LocalProducao> lp = m_controller.getListLocaisProducao();
        Utils.apresentaItem(lp, "", 9);

        FicheiroTipoMod();
        FicheiroTipoDat();
        FicheiroTipoRun();
        NeosServer.estabelecerLigacao("Results.txt");

    }

    private void FicheiroTipoDat() throws FileNotFoundException, ArrayIndexOutOfBoundsException {
        
        List<LocalProducao> lp = m_controller.getListLocaisProducao();
        List<MateriaPrima> mp = m_controller.getListMateriasPrimas();
        List<LocalDestino> ld = m_controller.getListLocaisDestino();
        List<LocalEspecializado> le = m_controller.getListLocaisEspecializados();
        List<ProdutoDerivado> pd = m_controller.getListProdsDerivados();
        List<PrecoVenda> pv = m_controller.getListPrecoVenda();
        List<Servico> se = m_controller.getListServicos();        
        List<CustoTransporte1> ct1 = m_controller.getListCustoTransporte1();
        List<CustoTransporte> ct = m_controller.getListCustoTransporte();

        String termMax= Utils.readLineFromConsole("Insira o Número Máximo de Terminais Abertos : "); 
        boolean bNew = true;
        do{
        String remove = Utils.readLineFromConsole("Pretende remover algum Local,matéria-prima,produto derivado ou Serviço? (S/N)");
        char q = remove.charAt(0);
        if(q == 's' || q == 'S'){
            int a = Utils.IntFromConsole("1) Remover Local Origem\n2) Remover Local Destino\n3) Remover Local Especializado\n4) Remover Matéria-Prima\n5) Remover Produto Derivado\n6) Remover Serviço");
            if(a == 1){
                Utils.apresentaLista(lp, "");
                String b= Utils.readLineFromConsole("Introduza a abreviatura do Local que pretende remover,em maiúsculas:");
                Utils.apresentaItem(lp, "", 0).replaceAll(b, "");
            }
        } else{
            System.out.println("Alterações Concluidas.");
        }
        }while(bNew);
       

        PrintWriter fd = new PrintWriter(new FileOutputStream("AMPL.dat"));
        String paragrafo = System.getProperty("line.separator");
        fd.write("#Locais de Origem:" + paragrafo
                + "set I:= " + Utils.apresentaItem(lp, "", 0) + ";" + paragrafo
                + "#Locais Especializados:" + paragrafo
                + "set J:=" + Utils.apresentaItem(le, "", 0) + ";" + paragrafo
                + "#Locais de Destino:" + paragrafo
                + "set K:=" + Utils.apresentaItem(ld, "", 0) + ";" + paragrafo
                + "#Materias-Primas:" + paragrafo
                + "set M:=" + Utils.apresentaItem(mp, "", 0) + ";" + paragrafo
                + "#Produtos Derivados" + paragrafo
                + "set Q:=" + Utils.apresentaItem(pd, "", 0) + ";" + paragrafo
                + "#Servicos" + paragrafo
                + "set S:=" + Utils.apresentaItem(se, "", 0) + ";" + paragrafo
                + paragrafo
                + "#Numero Maximo de terminais abertos" + paragrafo
                + "param Z:=" + termMax + ";" + paragrafo
                + "#Preço do produto q in Q vendido ao destino k in K" + paragrafo
                + "param P:=" + paragrafo + Utils.apresentaItem(pv, "",3) + ";" + paragrafo + paragrafo 
                + "#Custo de transporte das materias-primas in M da origem i in I para o terminal j in J " + paragrafo
                + "param Cin:=" + paragrafo + Utils.apresentaItem(ct, "",4) + ";" + paragrafo + paragrafo 
                + "#Custo de transporte do produto derivado q in Q, do terminal especializado j in J para o destino k in K" + paragrafo
                + "param Cout:=" + paragrafo + Utils.apresentaItem(ct1, "", 3) + paragrafo + paragrafo 
                + "#Custo de instalação de um local especializado j in J" + paragrafo
                + "param f:=" + paragrafo + Utils.apresentaItem(le, "", 2) + paragrafo 
                + "#Custo de instalação no local especializado" + paragrafo
                + "param gserv:=" + paragrafo + Utils.apresentaItem(le, "", 2) + ";" + paragrafo + paragrafo
                + "#Custo para abrir serviço no local especializado" + paragrafo
                + "param alphaserv:=" + ";" + paragrafo + paragrafo 
                + "#Racio" + paragrafo
                + "param betaserv default 0 :=" + paragrafo
                + "#Capacidade Nominal do Serviço no Local Especializado" + paragrafo
                + "param w:=" + paragrafo
                + "#Capacidade Total no Local Especializado" + paragrafo
                + "param W:=" + paragrafo + Utils.apresentaItem(le, "", 9) + ";" + paragrafo + paragrafo
                + "#Oferta" + paragrafo
                + "param: Sup:=" + paragrafo + paragrafo
                + "#Procura" + paragrafo
                + "param Dem:=" + paragrafo + paragrafo);

        fd.close();

        System.out.println("Ficheiro .dat concluido.");
        System.out.println("A estabelecer ligação ao Servidor. Pode demorar alguns segundos...");
    }

    private void FicheiroTipoMod() throws FileNotFoundException {

        PrintWriter fm = new PrintWriter(new FileOutputStream("AMPL.mod"));
        String paragrafo = System.getProperty("line.separator");

        fm.write("#Sets" + paragrafo + "set I; #conjunto de pontos de origem i " + paragrafo + "set J; #conjunto de terminais especializados j " + paragrafo
                + "set K; #conjunto de pontos com destino k" + paragrafo + "set M; #conjunto de inputs m" + paragrafo + "set Q; #conjunto de produtos q" + paragrafo + "set S; #conjunto de serviços s" + paragrafo + paragrafo
                + "#Parametros" + paragrafo + "param P{K,Q}; #Price of the product q in Q sold to destination k in K" + paragrafo
                + "param Cin{I,J,M}; #Cost of transporting input m in M from origin i in I to the terminal j in J" + paragrafo
                + "param Cout{J,K,Q}; #Cost of the transporting the product q in Q from the specialized terminal j in J to destination k in K" + paragrafo
                + "param f{J}; #Fixed cost of installation of a specialized terminal j in J" + paragrafo
                + "param gserv{J,S}; #Fixed cost to open the service s in S in the specialized terminal j in J" + paragrafo
                + "param alphaserv{J,S,M}; #Variable cost of the package of services s in S, applied to the input m in M in the specialized terminal j in J" + paragrafo
                + "param betaserv{J,S,M,Q}; #Change of volume coefficient by service s in S and input m in M" + paragrafo
                + "param w{J,S}; #Nominal service capacity of the service type s in S in terminal j in J" + paragrafo
                + "param W{J}; #Total capacity in the specialized terminal j in J" + paragrafo
                + "param Z; #Maximum number of opened specialized terminals" + paragrafo
                + "param Sup{I,M}; #Supply of inputs m in M at the origin i in I" + paragrafo
                + "param Dem{K,Q}; #Maximum demand for the product q in Q at destination k in K" + paragrafo
                + "" + paragrafo
                + "#Variaveis de decisao" + paragrafo
                + "var Y{J} binary; # 1, if the specialized terminal is opened in location j; 0, otherwise" + paragrafo
                + "var Yserv{J,S} binary ; # 1, if the service s is offered at the location j; 0, otherwise" + paragrafo
                + "var Xin{I,J,S,M} >=0; # Quantity of the input m, assigned from origin i to the specialized terminal j, to receive the service s" + paragrafo
                + "var Xout{J,K,S,Q} >=0; # Quantity of product q that was subjected to the service s in the specialized terminal j with destination k" + paragrafo + "" + paragrafo
                + "#Objective - Sell revenew - Transportations costs from origin to terminal - Costs for specialized terminal and services - Transportations costs from origin to terminal" + paragrafo + "" + paragrafo
                + "maximize Total_Profit: sum{k in K, q in Q, j in J, s in S} P[k,q]*Xout[j,k,s,q] " + paragrafo
                + "- sum{j in J}(sum{i in I, m in M}(Cin[i,j,m] * sum{s in S} Xin[i,j,s,m]))" + paragrafo
                + "- sum{j in J}(f[j]*Y[j]+sum{s in S}(gserv[j,s] * Yserv[j,s] + sum{m in M}( alphaserv[j,s,m]*sum{i in I} Xin[i,j,s,m])))" + paragrafo
                + "- sum{j in J,k in K, q in Q} ( Cout[j,k,q] * sum{s in S} Xout[j,k,s,q]);" + paragrafo
                + " " + paragrafo
                + "#Restricoes" + paragrafo + paragrafo
                + "#every input m supplied by every origin i directed to all services sin S in all terminals j in J " + paragrafo
                + "# must be compatible with the supply of this input m at the given origin i." + paragrafo
                + "subject to  InputSup {i in I, m in M}: sum{j in J, s in S} Xin[i,j,s,m] <= Sup[i,m]; " + paragrafo + paragrafo
                + "# the quantity of product q from all terminals jin J must be equal or" + paragrafo + "# lower than the maximum demand of product q at destination k" + paragrafo
                + "subject to  ProdDem {k in K, q in Q} : sum{j in J, s in S} Xout[j,k,s,q] <= Dem[k,q];" + paragrafo + paragrafo
                + "# for each terminal j and each service package s the inbound flow of input m sent to such terminal " + paragrafo + "# must be compatible with the nominal service capacity locally offered" + paragrafo
                + "subject to  TermServCap {j in J, s in S}: sum{i in I, m in M} Xin[i,j,s,m] <= w[j,s]*Yserv[j,s];" + paragrafo + paragrafo
                + "# the totalinputs m sent from all origins i to any given service s in terminals j " + paragrafo + "# must be compatible with the capacity of the corresponding terminal j" + paragrafo
                + "subject to  TermCap {j in J}: sum{i in I, s in S, m in M} Xin[i,j,s,m] <= W[j]*Y[j];" + paragrafo + paragrafo
                + "# each input, each product, and each type of service the flow conservation equation " + paragrafo + "# indicating the equilibrium between all amounts received and sent from the corresponding terminals" + paragrafo
                + "subject to  Balance {j in J, s in S, m in M, q in Q}: betaserv[j,s,m,q]*sum{i in I} Xin[i,j,s,m] = sum{k in K} Xout[j,k,s,q];" + paragrafo + paragrafo
                + "# limit of Z terminals to be opened" + paragrafo
                + "subject to  TermNum: sum{j in J} Y[j] <= Z;");

        fm.close();
        System.out.println("Ficheiro .mod concluido.");
    }

    private void FicheiroTipoRun() throws FileNotFoundException {

        PrintWriter fr = new PrintWriter(new FileOutputStream("AMPL.run"));
        String paragrafo = System.getProperty("line.separator");
        fr.write("#option solver gurobi;" + paragrafo
                + "solve" + paragrafo
                + "# display variables" + paragrafo
                + "option display_precision -1;" + paragrafo + paragrafo
                + "display Xin, Xout;" + paragrafo + "display Y, Yserv;");

        fr.close();
        System.out.println("Ficheiro .run concluido.");
    }

}
